import { Injectable, NgModule, Component, ViewEncapsulation, ViewChild, Input, EventEmitter, Output, defineInjectable } from '@angular/core';
import Cropper from 'cropperjs';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class AngularCropperjsService {
    constructor() { }
}
AngularCropperjsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
AngularCropperjsService.ctorParameters = () => [];
/** @nocollapse */ AngularCropperjsService.ngInjectableDef = defineInjectable({ factory: function AngularCropperjsService_Factory() { return new AngularCropperjsService(); }, token: AngularCropperjsService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class CropperComponent {
    constructor() {
        this.cropperOptions = {};
        this.export = new EventEmitter();
        this.ready = new EventEmitter();
        this.isLoading = true;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * Image loaded
     * @param {?} ev
     * @return {?}
     */
    imageLoaded(ev) {
        //
        // Unset load error state
        this.loadError = false;
        /** @type {?} */
        const image = /** @type {?} */ (ev.target);
        this.imageElement = image;
        //
        // Add crossOrigin?
        console.log('this.cropperOptions', this.cropperOptions);
        if (this.cropperOptions.checkCrossOrigin)
            image.crossOrigin = 'anonymous';
        //
        // Image on ready event
        image.addEventListener('ready', () => {
            //
            // Emit ready
            this.ready.emit(true);
            //
            // Unset loading state
            this.isLoading = false;
            //
            // Validate cropbox existance
            if (this.cropbox) {
                //
                // Set cropbox data
                this.cropper.setCropBoxData(this.cropbox);
            }
        });
        /** @type {?} */
        let aspectRatio = NaN;
        if (this.settings) {
            const { width, height } = this.settings;
            aspectRatio = width / height;
        }
        //
        // Set crop options
        // extend default with custom config
        this.cropperOptions = Object.assign({
            aspectRatio,
            movable: false,
            scalable: false,
            zoomable: false,
            viewMode: 1,
            checkCrossOrigin: true
        }, this.cropperOptions);
        //
        // Set cropperjs
        if (!this.cropper) {
            this.cropper = new Cropper(image, this.cropperOptions);
        }
    }
    /**
     * Image load error
     * @param {?} event
     * @return {?}
     */
    imageLoadError(event) {
        //
        // Set load error state
        this.loadError = true;
        //
        // Unset loading state
        this.isLoading = false;
    }
    /**
     * Export canvas
     * @param {?=} base64
     * @return {?}
     */
    exportCanvas(base64) {
        /** @type {?} */
        const imageData = this.cropper.getImageData();
        /** @type {?} */
        const cropData = this.cropper.getCropBoxData();
        /** @type {?} */
        const canvas = this.cropper.getCroppedCanvas();
        /** @type {?} */
        const data = { imageData, cropData };
        /** @type {?} */
        const promise = new Promise(resolve => {
            //
            // Validate base64
            if (base64) {
                //
                // Resolve promise with dataUrl
                return resolve({
                    dataUrl: canvas.toDataURL('image/png')
                });
            }
            canvas.toBlob(blob => resolve({ blob }));
        });
        //
        // Emit export data when promise is ready
        promise.then(res => {
            this.export.emit(Object.assign(data, res));
        });
    }
}
CropperComponent.decorators = [
    { type: Component, args: [{
                selector: 'angular-cropper',
                template: `<!-- CROPPER WRAPPER -->
<div class="cropper-wrapper">

    <!-- LOADING -->
    <div class="loading-block" *ngIf="isLoading">
        <div class="spinner"></div>
    </div>

    <!-- LOAD ERROR -->
    <div class="alert alert-warning" *ngIf="loadError">{{ loadImageErrorText }}</div>

    <!-- CROPPER -->
    <div class="cropper">
        <img #image alt="image" [src]="imageUrl" (load)="imageLoaded($event)" (error)="imageLoadError($event)" />
    </div>
</div>
`,
                styles: [`:host{display:block}.cropper img{max-width:100%;max-height:100%;height:auto}.cropper-wrapper{position:relative;min-height:80px}.cropper-wrapper .loading-block{position:absolute;top:0;left:0;width:100%;height:100%}.cropper-wrapper .loading-block .spinner{width:31px;height:31px;margin:0 auto;border:2px solid rgba(97,100,193,.98);border-radius:50%;border-left-color:transparent;border-right-color:transparent;-webkit-animation:425ms linear infinite cssload-spin;position:absolute;top:calc(50% - 15px);left:calc(50% - 15px);animation:425ms linear infinite cssload-spin}@-webkit-keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}/*!
 * Cropper.js v1.4.1
 * https://fengyuanchen.github.io/cropperjs
 *
 * Copyright 2015-present Chen Fengyuan
 * Released under the MIT license
 *
 * Date: 2018-07-15T09:54:43.167Z
 */.cropper-container{direction:ltr;font-size:0;line-height:0;position:relative;touch-action:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.cropper-container img{display:block;height:100%;image-orientation:0deg;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;width:100%}.cropper-canvas,.cropper-crop-box,.cropper-drag-box,.cropper-modal,.cropper-wrap-box{bottom:0;left:0;position:absolute;right:0;top:0}.cropper-canvas,.cropper-wrap-box{overflow:hidden}.cropper-drag-box{background-color:#fff;opacity:0}.cropper-modal{background-color:#000;opacity:.5}.cropper-view-box{display:block;height:100%;outline:#39f solid 1px;overflow:hidden;width:100%}.cropper-dashed{border:0 dashed #eee;display:block;opacity:.5;position:absolute}.cropper-dashed.dashed-h{border-bottom-width:1px;border-top-width:1px;height:calc(100% / 3);left:0;top:calc(100% / 3);width:100%}.cropper-dashed.dashed-v{border-left-width:1px;border-right-width:1px;height:100%;left:calc(100% / 3);top:0;width:calc(100% / 3)}.cropper-center{display:block;height:0;left:50%;opacity:.75;position:absolute;top:50%;width:0}.cropper-center:after,.cropper-center:before{background-color:#eee;content:' ';display:block;position:absolute}.cropper-center:before{height:1px;left:-3px;top:0;width:7px}.cropper-center:after{height:7px;left:0;top:-3px;width:1px}.cropper-face,.cropper-line,.cropper-point{display:block;height:100%;opacity:.1;position:absolute;width:100%}.cropper-face{background-color:#fff;left:0;top:0}.cropper-line{background-color:#39f}.cropper-line.line-e{cursor:ew-resize;right:-3px;top:0;width:5px}.cropper-line.line-n{cursor:ns-resize;height:5px;left:0;top:-3px}.cropper-line.line-w{cursor:ew-resize;left:-3px;top:0;width:5px}.cropper-line.line-s{bottom:-3px;cursor:ns-resize;height:5px;left:0}.cropper-point{background-color:#39f;height:5px;opacity:.75;width:5px}.cropper-point.point-e{cursor:ew-resize;margin-top:-3px;right:-3px;top:50%}.cropper-point.point-n{cursor:ns-resize;left:50%;margin-left:-3px;top:-3px}.cropper-point.point-w{cursor:ew-resize;left:-3px;margin-top:-3px;top:50%}.cropper-point.point-s{bottom:-3px;cursor:s-resize;left:50%;margin-left:-3px}.cropper-point.point-ne{cursor:nesw-resize;right:-3px;top:-3px}.cropper-point.point-nw{cursor:nwse-resize;left:-3px;top:-3px}.cropper-point.point-sw{bottom:-3px;cursor:nesw-resize;left:-3px}.cropper-point.point-se{bottom:-3px;cursor:nwse-resize;height:20px;opacity:1;right:-3px;width:20px}@media (min-width:768px){.cropper-point.point-se{height:15px;width:15px}}@media (min-width:992px){.cropper-point.point-se{height:10px;width:10px}}@media (min-width:1200px){.cropper-point.point-se{height:5px;opacity:.75;width:5px}}.cropper-point.point-se:before{background-color:#39f;bottom:-50%;content:' ';display:block;height:200%;opacity:0;position:absolute;right:-50%;width:200%}.cropper-invisible{opacity:0}.cropper-bg{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAAA3NCSVQICAjb4U/gAAAABlBMVEXMzMz////TjRV2AAAACXBIWXMAAArrAAAK6wGCiw1aAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABFJREFUCJlj+M/AgBVhF/0PAH6/D/HkDxOGAAAAAElFTkSuQmCC)}.cropper-hide{display:block;height:0;position:absolute;width:0}.cropper-hidden{display:none!important}.cropper-move{cursor:move}.cropper-crop{cursor:crosshair}.cropper-disabled .cropper-drag-box,.cropper-disabled .cropper-face,.cropper-disabled .cropper-line,.cropper-disabled .cropper-point{cursor:not-allowed}`],
                encapsulation: ViewEncapsulation.None
            },] },
];
/** @nocollapse */
CropperComponent.ctorParameters = () => [];
CropperComponent.propDecorators = {
    image: [{ type: ViewChild, args: ['image',] }],
    imageUrl: [{ type: Input }],
    settings: [{ type: Input }],
    cropbox: [{ type: Input }],
    loadImageErrorText: [{ type: Input }],
    cropperOptions: [{ type: Input }],
    export: [{ type: Output }],
    ready: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class AngularCropperjsModule {
}
AngularCropperjsModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [CropperComponent],
                exports: [CropperComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { AngularCropperjsService, CropperComponent, AngularCropperjsModule };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1jcm9wcGVyanMuanMubWFwIiwic291cmNlcyI6WyJuZzovL2FuZ3VsYXItY3JvcHBlcmpzL2xpYi9hbmd1bGFyLWNyb3BwZXJqcy5zZXJ2aWNlLnRzIiwibmc6Ly9hbmd1bGFyLWNyb3BwZXJqcy9saWIvY3JvcHBlci9jcm9wcGVyLmNvbXBvbmVudC50cyIsIm5nOi8vYW5ndWxhci1jcm9wcGVyanMvbGliL2FuZ3VsYXItY3JvcHBlcmpzLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIEFuZ3VsYXJDcm9wcGVyanNTZXJ2aWNlIHtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBFbGVtZW50UmVmLCBWaWV3Q2hpbGQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IENyb3BwZXIgZnJvbSAnY3JvcHBlcmpzJztcblxuZXhwb3J0IGludGVyZmFjZSBJbWFnZUNyb3BwZXJTZXR0aW5nIHtcbiAgICB3aWR0aDogbnVtYmVyO1xuICAgIGhlaWdodDogbnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEltYWdlQ3JvcHBlclJlc3VsdCB7XG4gICAgaW1hZ2VEYXRhOiBDcm9wcGVyLkltYWdlRGF0YTtcbiAgICBjcm9wRGF0YTogQ3JvcHBlci5Dcm9wQm94RGF0YTtcbiAgICBibG9iPzogQmxvYjtcbiAgICBkYXRhVXJsPzogc3RyaW5nO1xufVxuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FuZ3VsYXItY3JvcHBlcicsXG4gICAgdGVtcGxhdGU6IGA8IS0tIENST1BQRVIgV1JBUFBFUiAtLT5cbjxkaXYgY2xhc3M9XCJjcm9wcGVyLXdyYXBwZXJcIj5cblxuICAgIDwhLS0gTE9BRElORyAtLT5cbiAgICA8ZGl2IGNsYXNzPVwibG9hZGluZy1ibG9ja1wiICpuZ0lmPVwiaXNMb2FkaW5nXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJzcGlubmVyXCI+PC9kaXY+XG4gICAgPC9kaXY+XG5cbiAgICA8IS0tIExPQUQgRVJST1IgLS0+XG4gICAgPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXdhcm5pbmdcIiAqbmdJZj1cImxvYWRFcnJvclwiPnt7IGxvYWRJbWFnZUVycm9yVGV4dCB9fTwvZGl2PlxuXG4gICAgPCEtLSBDUk9QUEVSIC0tPlxuICAgIDxkaXYgY2xhc3M9XCJjcm9wcGVyXCI+XG4gICAgICAgIDxpbWcgI2ltYWdlIGFsdD1cImltYWdlXCIgW3NyY109XCJpbWFnZVVybFwiIChsb2FkKT1cImltYWdlTG9hZGVkKCRldmVudClcIiAoZXJyb3IpPVwiaW1hZ2VMb2FkRXJyb3IoJGV2ZW50KVwiIC8+XG4gICAgPC9kaXY+XG48L2Rpdj5cbmAsXG4gICAgc3R5bGVzOiBbYDpob3N0e2Rpc3BsYXk6YmxvY2t9LmNyb3BwZXIgaW1ne21heC13aWR0aDoxMDAlO21heC1oZWlnaHQ6MTAwJTtoZWlnaHQ6YXV0b30uY3JvcHBlci13cmFwcGVye3Bvc2l0aW9uOnJlbGF0aXZlO21pbi1oZWlnaHQ6ODBweH0uY3JvcHBlci13cmFwcGVyIC5sb2FkaW5nLWJsb2Nre3Bvc2l0aW9uOmFic29sdXRlO3RvcDowO2xlZnQ6MDt3aWR0aDoxMDAlO2hlaWdodDoxMDAlfS5jcm9wcGVyLXdyYXBwZXIgLmxvYWRpbmctYmxvY2sgLnNwaW5uZXJ7d2lkdGg6MzFweDtoZWlnaHQ6MzFweDttYXJnaW46MCBhdXRvO2JvcmRlcjoycHggc29saWQgcmdiYSg5NywxMDAsMTkzLC45OCk7Ym9yZGVyLXJhZGl1czo1MCU7Ym9yZGVyLWxlZnQtY29sb3I6dHJhbnNwYXJlbnQ7Ym9yZGVyLXJpZ2h0LWNvbG9yOnRyYW5zcGFyZW50Oy13ZWJraXQtYW5pbWF0aW9uOjQyNW1zIGxpbmVhciBpbmZpbml0ZSBjc3Nsb2FkLXNwaW47cG9zaXRpb246YWJzb2x1dGU7dG9wOmNhbGMoNTAlIC0gMTVweCk7bGVmdDpjYWxjKDUwJSAtIDE1cHgpO2FuaW1hdGlvbjo0MjVtcyBsaW5lYXIgaW5maW5pdGUgY3NzbG9hZC1zcGlufUAtd2Via2l0LWtleWZyYW1lcyBjc3Nsb2FkLXNwaW57dG97LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDM2MGRlZyk7dHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpfX1Aa2V5ZnJhbWVzIGNzc2xvYWQtc3Bpbnt0b3std2Via2l0LXRyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKTt0cmFuc2Zvcm06cm90YXRlKDM2MGRlZyl9fS8qIVxuICogQ3JvcHBlci5qcyB2MS40LjFcbiAqIGh0dHBzOi8vZmVuZ3l1YW5jaGVuLmdpdGh1Yi5pby9jcm9wcGVyanNcbiAqXG4gKiBDb3B5cmlnaHQgMjAxNS1wcmVzZW50IENoZW4gRmVuZ3l1YW5cbiAqIFJlbGVhc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZVxuICpcbiAqIERhdGU6IDIwMTgtMDctMTVUMDk6NTQ6NDMuMTY3WlxuICovLmNyb3BwZXItY29udGFpbmVye2RpcmVjdGlvbjpsdHI7Zm9udC1zaXplOjA7bGluZS1oZWlnaHQ6MDtwb3NpdGlvbjpyZWxhdGl2ZTt0b3VjaC1hY3Rpb246bm9uZTstd2Via2l0LXVzZXItc2VsZWN0Om5vbmU7LW1vei11c2VyLXNlbGVjdDpub25lOy1tcy11c2VyLXNlbGVjdDpub25lO3VzZXItc2VsZWN0Om5vbmV9LmNyb3BwZXItY29udGFpbmVyIGltZ3tkaXNwbGF5OmJsb2NrO2hlaWdodDoxMDAlO2ltYWdlLW9yaWVudGF0aW9uOjBkZWc7bWF4LWhlaWdodDpub25lIWltcG9ydGFudDttYXgtd2lkdGg6bm9uZSFpbXBvcnRhbnQ7bWluLWhlaWdodDowIWltcG9ydGFudDttaW4td2lkdGg6MCFpbXBvcnRhbnQ7d2lkdGg6MTAwJX0uY3JvcHBlci1jYW52YXMsLmNyb3BwZXItY3JvcC1ib3gsLmNyb3BwZXItZHJhZy1ib3gsLmNyb3BwZXItbW9kYWwsLmNyb3BwZXItd3JhcC1ib3h7Ym90dG9tOjA7bGVmdDowO3Bvc2l0aW9uOmFic29sdXRlO3JpZ2h0OjA7dG9wOjB9LmNyb3BwZXItY2FudmFzLC5jcm9wcGVyLXdyYXAtYm94e292ZXJmbG93OmhpZGRlbn0uY3JvcHBlci1kcmFnLWJveHtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7b3BhY2l0eTowfS5jcm9wcGVyLW1vZGFse2JhY2tncm91bmQtY29sb3I6IzAwMDtvcGFjaXR5Oi41fS5jcm9wcGVyLXZpZXctYm94e2Rpc3BsYXk6YmxvY2s7aGVpZ2h0OjEwMCU7b3V0bGluZTojMzlmIHNvbGlkIDFweDtvdmVyZmxvdzpoaWRkZW47d2lkdGg6MTAwJX0uY3JvcHBlci1kYXNoZWR7Ym9yZGVyOjAgZGFzaGVkICNlZWU7ZGlzcGxheTpibG9jaztvcGFjaXR5Oi41O3Bvc2l0aW9uOmFic29sdXRlfS5jcm9wcGVyLWRhc2hlZC5kYXNoZWQtaHtib3JkZXItYm90dG9tLXdpZHRoOjFweDtib3JkZXItdG9wLXdpZHRoOjFweDtoZWlnaHQ6Y2FsYygxMDAlIC8gMyk7bGVmdDowO3RvcDpjYWxjKDEwMCUgLyAzKTt3aWR0aDoxMDAlfS5jcm9wcGVyLWRhc2hlZC5kYXNoZWQtdntib3JkZXItbGVmdC13aWR0aDoxcHg7Ym9yZGVyLXJpZ2h0LXdpZHRoOjFweDtoZWlnaHQ6MTAwJTtsZWZ0OmNhbGMoMTAwJSAvIDMpO3RvcDowO3dpZHRoOmNhbGMoMTAwJSAvIDMpfS5jcm9wcGVyLWNlbnRlcntkaXNwbGF5OmJsb2NrO2hlaWdodDowO2xlZnQ6NTAlO29wYWNpdHk6Ljc1O3Bvc2l0aW9uOmFic29sdXRlO3RvcDo1MCU7d2lkdGg6MH0uY3JvcHBlci1jZW50ZXI6YWZ0ZXIsLmNyb3BwZXItY2VudGVyOmJlZm9yZXtiYWNrZ3JvdW5kLWNvbG9yOiNlZWU7Y29udGVudDonICc7ZGlzcGxheTpibG9jaztwb3NpdGlvbjphYnNvbHV0ZX0uY3JvcHBlci1jZW50ZXI6YmVmb3Jle2hlaWdodDoxcHg7bGVmdDotM3B4O3RvcDowO3dpZHRoOjdweH0uY3JvcHBlci1jZW50ZXI6YWZ0ZXJ7aGVpZ2h0OjdweDtsZWZ0OjA7dG9wOi0zcHg7d2lkdGg6MXB4fS5jcm9wcGVyLWZhY2UsLmNyb3BwZXItbGluZSwuY3JvcHBlci1wb2ludHtkaXNwbGF5OmJsb2NrO2hlaWdodDoxMDAlO29wYWNpdHk6LjE7cG9zaXRpb246YWJzb2x1dGU7d2lkdGg6MTAwJX0uY3JvcHBlci1mYWNle2JhY2tncm91bmQtY29sb3I6I2ZmZjtsZWZ0OjA7dG9wOjB9LmNyb3BwZXItbGluZXtiYWNrZ3JvdW5kLWNvbG9yOiMzOWZ9LmNyb3BwZXItbGluZS5saW5lLWV7Y3Vyc29yOmV3LXJlc2l6ZTtyaWdodDotM3B4O3RvcDowO3dpZHRoOjVweH0uY3JvcHBlci1saW5lLmxpbmUtbntjdXJzb3I6bnMtcmVzaXplO2hlaWdodDo1cHg7bGVmdDowO3RvcDotM3B4fS5jcm9wcGVyLWxpbmUubGluZS13e2N1cnNvcjpldy1yZXNpemU7bGVmdDotM3B4O3RvcDowO3dpZHRoOjVweH0uY3JvcHBlci1saW5lLmxpbmUtc3tib3R0b206LTNweDtjdXJzb3I6bnMtcmVzaXplO2hlaWdodDo1cHg7bGVmdDowfS5jcm9wcGVyLXBvaW50e2JhY2tncm91bmQtY29sb3I6IzM5ZjtoZWlnaHQ6NXB4O29wYWNpdHk6Ljc1O3dpZHRoOjVweH0uY3JvcHBlci1wb2ludC5wb2ludC1le2N1cnNvcjpldy1yZXNpemU7bWFyZ2luLXRvcDotM3B4O3JpZ2h0Oi0zcHg7dG9wOjUwJX0uY3JvcHBlci1wb2ludC5wb2ludC1ue2N1cnNvcjpucy1yZXNpemU7bGVmdDo1MCU7bWFyZ2luLWxlZnQ6LTNweDt0b3A6LTNweH0uY3JvcHBlci1wb2ludC5wb2ludC13e2N1cnNvcjpldy1yZXNpemU7bGVmdDotM3B4O21hcmdpbi10b3A6LTNweDt0b3A6NTAlfS5jcm9wcGVyLXBvaW50LnBvaW50LXN7Ym90dG9tOi0zcHg7Y3Vyc29yOnMtcmVzaXplO2xlZnQ6NTAlO21hcmdpbi1sZWZ0Oi0zcHh9LmNyb3BwZXItcG9pbnQucG9pbnQtbmV7Y3Vyc29yOm5lc3ctcmVzaXplO3JpZ2h0Oi0zcHg7dG9wOi0zcHh9LmNyb3BwZXItcG9pbnQucG9pbnQtbnd7Y3Vyc29yOm53c2UtcmVzaXplO2xlZnQ6LTNweDt0b3A6LTNweH0uY3JvcHBlci1wb2ludC5wb2ludC1zd3tib3R0b206LTNweDtjdXJzb3I6bmVzdy1yZXNpemU7bGVmdDotM3B4fS5jcm9wcGVyLXBvaW50LnBvaW50LXNle2JvdHRvbTotM3B4O2N1cnNvcjpud3NlLXJlc2l6ZTtoZWlnaHQ6MjBweDtvcGFjaXR5OjE7cmlnaHQ6LTNweDt3aWR0aDoyMHB4fUBtZWRpYSAobWluLXdpZHRoOjc2OHB4KXsuY3JvcHBlci1wb2ludC5wb2ludC1zZXtoZWlnaHQ6MTVweDt3aWR0aDoxNXB4fX1AbWVkaWEgKG1pbi13aWR0aDo5OTJweCl7LmNyb3BwZXItcG9pbnQucG9pbnQtc2V7aGVpZ2h0OjEwcHg7d2lkdGg6MTBweH19QG1lZGlhIChtaW4td2lkdGg6MTIwMHB4KXsuY3JvcHBlci1wb2ludC5wb2ludC1zZXtoZWlnaHQ6NXB4O29wYWNpdHk6Ljc1O3dpZHRoOjVweH19LmNyb3BwZXItcG9pbnQucG9pbnQtc2U6YmVmb3Jle2JhY2tncm91bmQtY29sb3I6IzM5Zjtib3R0b206LTUwJTtjb250ZW50OicgJztkaXNwbGF5OmJsb2NrO2hlaWdodDoyMDAlO29wYWNpdHk6MDtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDotNTAlO3dpZHRoOjIwMCV9LmNyb3BwZXItaW52aXNpYmxle29wYWNpdHk6MH0uY3JvcHBlci1iZ3tiYWNrZ3JvdW5kLWltYWdlOnVybChkYXRhOmltYWdlL3BuZztiYXNlNjQsaVZCT1J3MEtHZ29BQUFBTlNVaEVVZ0FBQUJBQUFBQVFBUU1BQUFBbFBXMGlBQUFBQTNOQ1NWUUlDQWpiNFUvZ0FBQUFCbEJNVkVYTXpNei8vLy9UalJWMkFBQUFDWEJJV1hNQUFBcnJBQUFLNndHQ2l3MWFBQUFBSEhSRldIUlRiMlowZDJGeVpRQkJaRzlpWlNCR2FYSmxkMjl5YTNNZ1ExTTI2THl5akFBQUFCRkpSRUZVQ0psaitNL0FnQlZoRi8wUEFINi9EL0hrRHhPR0FBQUFBRWxGVGtTdVFtQ0MpfS5jcm9wcGVyLWhpZGV7ZGlzcGxheTpibG9jaztoZWlnaHQ6MDtwb3NpdGlvbjphYnNvbHV0ZTt3aWR0aDowfS5jcm9wcGVyLWhpZGRlbntkaXNwbGF5Om5vbmUhaW1wb3J0YW50fS5jcm9wcGVyLW1vdmV7Y3Vyc29yOm1vdmV9LmNyb3BwZXItY3JvcHtjdXJzb3I6Y3Jvc3NoYWlyfS5jcm9wcGVyLWRpc2FibGVkIC5jcm9wcGVyLWRyYWctYm94LC5jcm9wcGVyLWRpc2FibGVkIC5jcm9wcGVyLWZhY2UsLmNyb3BwZXItZGlzYWJsZWQgLmNyb3BwZXItbGluZSwuY3JvcHBlci1kaXNhYmxlZCAuY3JvcHBlci1wb2ludHtjdXJzb3I6bm90LWFsbG93ZWR9YF0sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBDcm9wcGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBWaWV3Q2hpbGQoJ2ltYWdlJykgaW1hZ2U6IEVsZW1lbnRSZWY7XG5cbiAgICBASW5wdXQoKSBpbWFnZVVybDogYW55O1xuICAgIEBJbnB1dCgpIHNldHRpbmdzOiBJbWFnZUNyb3BwZXJTZXR0aW5nO1xuICAgIEBJbnB1dCgpIGNyb3Bib3g6IENyb3BwZXIuQ3JvcEJveERhdGE7XG4gICAgQElucHV0KCkgbG9hZEltYWdlRXJyb3JUZXh0OiBzdHJpbmc7XG4gICAgQElucHV0KCkgY3JvcHBlck9wdGlvbnM6IGFueSA9IHt9O1xuXG4gICAgQE91dHB1dCgpIGV4cG9ydCA9IG5ldyBFdmVudEVtaXR0ZXI8SW1hZ2VDcm9wcGVyUmVzdWx0PigpO1xuICAgIEBPdXRwdXQoKSByZWFkeSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIHB1YmxpYyBpc0xvYWRpbmc6IGJvb2xlYW4gPSB0cnVlO1xuICAgIHB1YmxpYyBjcm9wcGVyOiBDcm9wcGVyO1xuICAgIHB1YmxpYyBpbWFnZUVsZW1lbnQ6IEhUTUxJbWFnZUVsZW1lbnQ7XG4gICAgcHVibGljIGxvYWRFcnJvcjogYW55O1xuXG4gICAgY29uc3RydWN0b3IoKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEltYWdlIGxvYWRlZFxuICAgICAqIEBwYXJhbSBldlxuICAgICAqL1xuICAgIGltYWdlTG9hZGVkKGV2OiBFdmVudCkge1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFVuc2V0IGxvYWQgZXJyb3Igc3RhdGVcbiAgICAgICAgdGhpcy5sb2FkRXJyb3IgPSBmYWxzZTtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBTZXR1cCBpbWFnZSBlbGVtZW50XG4gICAgICAgIGNvbnN0IGltYWdlID0gZXYudGFyZ2V0IGFzIEhUTUxJbWFnZUVsZW1lbnQ7XG4gICAgICAgIHRoaXMuaW1hZ2VFbGVtZW50ID0gaW1hZ2U7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gQWRkIGNyb3NzT3JpZ2luP1xuICAgICAgICBjb25zb2xlLmxvZygndGhpcy5jcm9wcGVyT3B0aW9ucycsIHRoaXMuY3JvcHBlck9wdGlvbnMpO1xuICAgICAgICBpZiAodGhpcy5jcm9wcGVyT3B0aW9ucy5jaGVja0Nyb3NzT3JpZ2luKSBpbWFnZS5jcm9zc09yaWdpbiA9ICdhbm9ueW1vdXMnO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEltYWdlIG9uIHJlYWR5IGV2ZW50XG4gICAgICAgIGltYWdlLmFkZEV2ZW50TGlzdGVuZXIoJ3JlYWR5JywgKCkgPT4ge1xuICAgICAgICAgICAgLy9cbiAgICAgICAgICAgIC8vIEVtaXQgcmVhZHlcbiAgICAgICAgICAgIHRoaXMucmVhZHkuZW1pdCh0cnVlKTtcblxuICAgICAgICAgICAgLy9cbiAgICAgICAgICAgIC8vIFVuc2V0IGxvYWRpbmcgc3RhdGVcbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2U7XG5cbiAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAvLyBWYWxpZGF0ZSBjcm9wYm94IGV4aXN0YW5jZVxuICAgICAgICAgICAgaWYgKHRoaXMuY3JvcGJveCkge1xuXG4gICAgICAgICAgICAgICAgLy9cbiAgICAgICAgICAgICAgICAvLyBTZXQgY3JvcGJveCBkYXRhXG4gICAgICAgICAgICAgICAgdGhpcy5jcm9wcGVyLnNldENyb3BCb3hEYXRhKHRoaXMuY3JvcGJveCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFNldHVwIGFzcGVjdCByYXRpbyBhY2NvcmRpbmcgdG8gc2V0dGluZ3NcbiAgICAgICAgbGV0IGFzcGVjdFJhdGlvID0gTmFOO1xuICAgICAgICBpZiAodGhpcy5zZXR0aW5ncykge1xuICAgICAgICAgICAgY29uc3QgeyB3aWR0aCwgaGVpZ2h0IH0gPSB0aGlzLnNldHRpbmdzO1xuICAgICAgICAgICAgYXNwZWN0UmF0aW8gPSB3aWR0aCAvIGhlaWdodDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFNldCBjcm9wIG9wdGlvbnNcbiAgICAgICAgLy8gZXh0ZW5kIGRlZmF1bHQgd2l0aCBjdXN0b20gY29uZmlnXG4gICAgICAgIHRoaXMuY3JvcHBlck9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHtcbiAgICAgICAgICAgIGFzcGVjdFJhdGlvLFxuICAgICAgICAgICAgbW92YWJsZTogZmFsc2UsXG4gICAgICAgICAgICBzY2FsYWJsZTogZmFsc2UsXG4gICAgICAgICAgICB6b29tYWJsZTogZmFsc2UsXG4gICAgICAgICAgICB2aWV3TW9kZTogMSxcbiAgICAgICAgICAgIGNoZWNrQ3Jvc3NPcmlnaW46IHRydWVcbiAgICAgICAgfSwgdGhpcy5jcm9wcGVyT3B0aW9ucyk7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gU2V0IGNyb3BwZXJqc1xuICAgICAgICB0aGlzLmNyb3BwZXIgPSBuZXcgQ3JvcHBlcihpbWFnZSwgdGhpcy5jcm9wcGVyT3B0aW9ucyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogSW1hZ2UgbG9hZCBlcnJvclxuICAgICAqIEBwYXJhbSBldmVudFxuICAgICAqL1xuICAgIGltYWdlTG9hZEVycm9yKGV2ZW50OiBhbnkpIHtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBTZXQgbG9hZCBlcnJvciBzdGF0ZVxuICAgICAgICB0aGlzLmxvYWRFcnJvciA9IHRydWU7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gVW5zZXQgbG9hZGluZyBzdGF0ZVxuICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEV4cG9ydCBjYW52YXNcbiAgICAgKiBAcGFyYW0gYmFzZTY0XG4gICAgICovXG4gICAgZXhwb3J0Q2FudmFzKGJhc2U2ND86IGFueSkge1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEdldCBhbmQgc2V0IGltYWdlLCBjcm9wIGFuZCBjYW52YXMgZGF0YVxuICAgICAgICBjb25zdCBpbWFnZURhdGEgPSB0aGlzLmNyb3BwZXIuZ2V0SW1hZ2VEYXRhKCk7XG4gICAgICAgIGNvbnN0IGNyb3BEYXRhID0gdGhpcy5jcm9wcGVyLmdldENyb3BCb3hEYXRhKCk7XG4gICAgICAgIGNvbnN0IGNhbnZhcyA9IHRoaXMuY3JvcHBlci5nZXRDcm9wcGVkQ2FudmFzKCk7XG4gICAgICAgIGNvbnN0IGRhdGEgPSB7IGltYWdlRGF0YSwgY3JvcERhdGEgfTtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBDcmVhdGUgcHJvbWlzZSB0byByZXNvbHZlIGNhbnZhcyBkYXRhXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSBuZXcgUHJvbWlzZShyZXNvbHZlID0+IHtcblxuICAgICAgICAgICAgLy9cbiAgICAgICAgICAgIC8vIFZhbGlkYXRlIGJhc2U2NFxuICAgICAgICAgICAgaWYgKGJhc2U2NCkge1xuXG4gICAgICAgICAgICAgICAgLy9cbiAgICAgICAgICAgICAgICAvLyBSZXNvbHZlIHByb21pc2Ugd2l0aCBkYXRhVXJsXG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc29sdmUoe1xuICAgICAgICAgICAgICAgICAgICBkYXRhVXJsOiBjYW52YXMudG9EYXRhVVJMKCdpbWFnZS9wbmcnKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2FudmFzLnRvQmxvYihibG9iID0+IHJlc29sdmUoeyBibG9iIH0pKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gRW1pdCBleHBvcnQgZGF0YSB3aGVuIHByb21pc2UgaXMgcmVhZHlcbiAgICAgICAgcHJvbWlzZS50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICB0aGlzLmV4cG9ydC5lbWl0KE9iamVjdC5hc3NpZ24oZGF0YSwgcmVzKSk7XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDcm9wcGVyQ29tcG9uZW50IH0gZnJvbSAnLi9jcm9wcGVyL2Nyb3BwZXIuY29tcG9uZW50JztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW1xuICAgICAgICBDb21tb25Nb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW0Nyb3BwZXJDb21wb25lbnRdLFxuICAgIGV4cG9ydHM6IFtDcm9wcGVyQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFyQ3JvcHBlcmpzTW9kdWxlIHsgfVxuIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7SUFPRSxpQkFBaUI7OztZQUxsQixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7Ozs7Ozs7Ozs7QUNKRDtJQStESTs4QkFWK0IsRUFBRTtzQkFFZCxJQUFJLFlBQVksRUFBc0I7cUJBQ3ZDLElBQUksWUFBWSxFQUFFO3lCQUVSLElBQUk7S0FLZjs7OztJQUVqQixRQUFRO0tBQ1A7Ozs7OztJQU1ELFdBQVcsQ0FBQyxFQUFTOzs7UUFJakIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7O1FBSXZCLE1BQU0sS0FBSyxxQkFBRyxFQUFFLENBQUMsTUFBMEIsRUFBQztRQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQzs7O1FBSTFCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3hELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0I7WUFBRSxLQUFLLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQzs7O1FBSTFFLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7OztZQUc1QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7O1lBSXRCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDOzs7WUFJdkIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFOzs7Z0JBSWQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzdDO1NBQ0osQ0FBQyxDQUFDOztRQUlILElBQUksV0FBVyxHQUFHLEdBQUcsQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZixNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDeEMsV0FBVyxHQUFHLEtBQUssR0FBRyxNQUFNLENBQUM7U0FDaEM7Ozs7UUFLRCxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7WUFDaEMsV0FBVztZQUNYLE9BQU8sRUFBRSxLQUFLO1lBQ2QsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFFBQVEsRUFBRSxDQUFDO1lBQ1gsZ0JBQWdCLEVBQUUsSUFBSTtTQUN6QixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs7O1FBSXhCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztLQUMxRDs7Ozs7O0lBTUQsY0FBYyxDQUFDLEtBQVU7OztRQUlyQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzs7O1FBSXRCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO0tBQzFCOzs7Ozs7SUFNRCxZQUFZLENBQUMsTUFBWTs7UUFJckIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQzs7UUFDOUMsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQzs7UUFDL0MsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDOztRQUMvQyxNQUFNLElBQUksR0FBRyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsQ0FBQzs7UUFJckMsTUFBTSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsT0FBTzs7O1lBSS9CLElBQUksTUFBTSxFQUFFOzs7Z0JBSVIsT0FBTyxPQUFPLENBQUM7b0JBQ1gsT0FBTyxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDO2lCQUN6QyxDQUFDLENBQUM7YUFDTjtZQUNELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM1QyxDQUFDLENBQUM7OztRQUlILE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRztZQUNaLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7U0FDOUMsQ0FBQyxDQUFDO0tBQ047OztZQXpLSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7O0NBZ0JiO2dCQUNHLE1BQU0sRUFBRSxDQUFDOzs7Ozs7OztnOEdBUW03RyxDQUFDO2dCQUM3N0csYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDeEM7Ozs7O29CQUdJLFNBQVMsU0FBQyxPQUFPO3VCQUVqQixLQUFLO3VCQUNMLEtBQUs7c0JBQ0wsS0FBSztpQ0FDTCxLQUFLOzZCQUNMLEtBQUs7cUJBRUwsTUFBTTtvQkFDTixNQUFNOzs7Ozs7O0FDeERYOzs7WUFJQyxRQUFRLFNBQUM7Z0JBQ04sT0FBTyxFQUFFO29CQUNMLFlBQVk7aUJBQ2Y7Z0JBQ0QsWUFBWSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2hDLE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO2FBQzlCOzs7Ozs7Ozs7Ozs7Ozs7In0=