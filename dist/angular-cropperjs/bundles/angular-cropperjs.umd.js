(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('cropperjs'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('angular-cropperjs', ['exports', '@angular/core', 'cropperjs', '@angular/common'], factory) :
    (factory((global['angular-cropperjs'] = {}),global.ng.core,null,global.ng.common));
}(this, (function (exports,i0,Cropper,common) { 'use strict';

    Cropper = Cropper && Cropper.hasOwnProperty('default') ? Cropper['default'] : Cropper;

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var AngularCropperjsService = (function () {
        function AngularCropperjsService() {
        }
        AngularCropperjsService.decorators = [
            { type: i0.Injectable, args: [{
                        providedIn: 'root'
                    },] },
        ];
        /** @nocollapse */
        AngularCropperjsService.ctorParameters = function () { return []; };
        /** @nocollapse */ AngularCropperjsService.ngInjectableDef = i0.defineInjectable({ factory: function AngularCropperjsService_Factory() { return new AngularCropperjsService(); }, token: AngularCropperjsService, providedIn: "root" });
        return AngularCropperjsService;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var CropperComponent = (function () {
        function CropperComponent() {
            this.cropperOptions = {};
            this.export = new i0.EventEmitter();
            this.ready = new i0.EventEmitter();
            this.isLoading = true;
        }
        /**
         * @return {?}
         */
        CropperComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
            };
        /**
         * Image loaded
         * @param ev
         */
        /**
         * Image loaded
         * @param {?} ev
         * @return {?}
         */
        CropperComponent.prototype.imageLoaded = /**
         * Image loaded
         * @param {?} ev
         * @return {?}
         */
            function (ev) {
                var _this = this;
                //
                // Unset load error state
                this.loadError = false;
                /** @type {?} */
                var image = (ev.target);
                this.imageElement = image;
                //
                // Add crossOrigin?
                console.log('this.cropperOptions', this.cropperOptions);
                if (this.cropperOptions.checkCrossOrigin)
                    image.crossOrigin = 'anonymous';
                //
                // Image on ready event
                image.addEventListener('ready', function () {
                    //
                    // Emit ready
                    //
                    // Emit ready
                    _this.ready.emit(true);
                    //
                    // Unset loading state
                    //
                    // Unset loading state
                    _this.isLoading = false;
                    //
                    // Validate cropbox existance
                    if (_this.cropbox) {
                        //
                        // Set cropbox data
                        //
                        // Set cropbox data
                        _this.cropper.setCropBoxData(_this.cropbox);
                    }
                });
                /** @type {?} */
                var aspectRatio = NaN;
                if (this.settings) {
                    var _a = this.settings, width = _a.width, height = _a.height;
                    aspectRatio = width / height;
                }
                //
                // Set crop options
                // extend default with custom config
                this.cropperOptions = Object.assign({
                    aspectRatio: aspectRatio,
                    movable: false,
                    scalable: false,
                    zoomable: false,
                    viewMode: 1,
                    checkCrossOrigin: true
                }, this.cropperOptions);
                //
                // Set cropperjs
                this.cropper = new Cropper(image, this.cropperOptions);
            };
        /**
         * Image load error
         * @param event
         */
        /**
         * Image load error
         * @param {?} event
         * @return {?}
         */
        CropperComponent.prototype.imageLoadError = /**
         * Image load error
         * @param {?} event
         * @return {?}
         */
            function (event) {
                //
                // Set load error state
                this.loadError = true;
                //
                // Unset loading state
                this.isLoading = false;
            };
        /**
         * Export canvas
         * @param base64
         */
        /**
         * Export canvas
         * @param {?=} base64
         * @return {?}
         */
        CropperComponent.prototype.exportCanvas = /**
         * Export canvas
         * @param {?=} base64
         * @return {?}
         */
            function (base64) {
                var _this = this;
                /** @type {?} */
                var imageData = this.cropper.getImageData();
                /** @type {?} */
                var cropData = this.cropper.getCropBoxData();
                /** @type {?} */
                var canvas = this.cropper.getCroppedCanvas();
                /** @type {?} */
                var data = { imageData: imageData, cropData: cropData };
                /** @type {?} */
                var promise = new Promise(function (resolve) {
                    //
                    // Validate base64
                    if (base64) {
                        //
                        // Resolve promise with dataUrl
                        return resolve({
                            dataUrl: canvas.toDataURL('image/png')
                        });
                    }
                    canvas.toBlob(function (blob) { return resolve({ blob: blob }); });
                });
                //
                // Emit export data when promise is ready
                promise.then(function (res) {
                    _this.export.emit(Object.assign(data, res));
                });
            };
        CropperComponent.decorators = [
            { type: i0.Component, args: [{
                        selector: 'angular-cropper',
                        template: "<!-- CROPPER WRAPPER -->\n<div class=\"cropper-wrapper\">\n\n    <!-- LOADING -->\n    <div class=\"loading-block\" *ngIf=\"isLoading\">\n        <div class=\"spinner\"></div>\n    </div>\n\n    <!-- LOAD ERROR -->\n    <div class=\"alert alert-warning\" *ngIf=\"loadError\">{{ loadImageErrorText }}</div>\n\n    <!-- CROPPER -->\n    <div class=\"cropper\">\n        <img #image alt=\"image\" [src]=\"imageUrl\" (load)=\"imageLoaded($event)\" (error)=\"imageLoadError($event)\" />\n    </div>\n</div>\n",
                        styles: [":host{display:block}.cropper img{max-width:100%;max-height:100%;height:auto}.cropper-wrapper{position:relative;min-height:80px}.cropper-wrapper .loading-block{position:absolute;top:0;left:0;width:100%;height:100%}.cropper-wrapper .loading-block .spinner{width:31px;height:31px;margin:0 auto;border:2px solid rgba(97,100,193,.98);border-radius:50%;border-left-color:transparent;border-right-color:transparent;-webkit-animation:425ms linear infinite cssload-spin;position:absolute;top:calc(50% - 15px);left:calc(50% - 15px);animation:425ms linear infinite cssload-spin}@-webkit-keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes cssload-spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}/*!\n * Cropper.js v1.4.1\n * https://fengyuanchen.github.io/cropperjs\n *\n * Copyright 2015-present Chen Fengyuan\n * Released under the MIT license\n *\n * Date: 2018-07-15T09:54:43.167Z\n */.cropper-container{direction:ltr;font-size:0;line-height:0;position:relative;touch-action:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.cropper-container img{display:block;height:100%;image-orientation:0deg;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;width:100%}.cropper-canvas,.cropper-crop-box,.cropper-drag-box,.cropper-modal,.cropper-wrap-box{bottom:0;left:0;position:absolute;right:0;top:0}.cropper-canvas,.cropper-wrap-box{overflow:hidden}.cropper-drag-box{background-color:#fff;opacity:0}.cropper-modal{background-color:#000;opacity:.5}.cropper-view-box{display:block;height:100%;outline:#39f solid 1px;overflow:hidden;width:100%}.cropper-dashed{border:0 dashed #eee;display:block;opacity:.5;position:absolute}.cropper-dashed.dashed-h{border-bottom-width:1px;border-top-width:1px;height:calc(100% / 3);left:0;top:calc(100% / 3);width:100%}.cropper-dashed.dashed-v{border-left-width:1px;border-right-width:1px;height:100%;left:calc(100% / 3);top:0;width:calc(100% / 3)}.cropper-center{display:block;height:0;left:50%;opacity:.75;position:absolute;top:50%;width:0}.cropper-center:after,.cropper-center:before{background-color:#eee;content:' ';display:block;position:absolute}.cropper-center:before{height:1px;left:-3px;top:0;width:7px}.cropper-center:after{height:7px;left:0;top:-3px;width:1px}.cropper-face,.cropper-line,.cropper-point{display:block;height:100%;opacity:.1;position:absolute;width:100%}.cropper-face{background-color:#fff;left:0;top:0}.cropper-line{background-color:#39f}.cropper-line.line-e{cursor:ew-resize;right:-3px;top:0;width:5px}.cropper-line.line-n{cursor:ns-resize;height:5px;left:0;top:-3px}.cropper-line.line-w{cursor:ew-resize;left:-3px;top:0;width:5px}.cropper-line.line-s{bottom:-3px;cursor:ns-resize;height:5px;left:0}.cropper-point{background-color:#39f;height:5px;opacity:.75;width:5px}.cropper-point.point-e{cursor:ew-resize;margin-top:-3px;right:-3px;top:50%}.cropper-point.point-n{cursor:ns-resize;left:50%;margin-left:-3px;top:-3px}.cropper-point.point-w{cursor:ew-resize;left:-3px;margin-top:-3px;top:50%}.cropper-point.point-s{bottom:-3px;cursor:s-resize;left:50%;margin-left:-3px}.cropper-point.point-ne{cursor:nesw-resize;right:-3px;top:-3px}.cropper-point.point-nw{cursor:nwse-resize;left:-3px;top:-3px}.cropper-point.point-sw{bottom:-3px;cursor:nesw-resize;left:-3px}.cropper-point.point-se{bottom:-3px;cursor:nwse-resize;height:20px;opacity:1;right:-3px;width:20px}@media (min-width:768px){.cropper-point.point-se{height:15px;width:15px}}@media (min-width:992px){.cropper-point.point-se{height:10px;width:10px}}@media (min-width:1200px){.cropper-point.point-se{height:5px;opacity:.75;width:5px}}.cropper-point.point-se:before{background-color:#39f;bottom:-50%;content:' ';display:block;height:200%;opacity:0;position:absolute;right:-50%;width:200%}.cropper-invisible{opacity:0}.cropper-bg{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAAA3NCSVQICAjb4U/gAAAABlBMVEXMzMz////TjRV2AAAACXBIWXMAAArrAAAK6wGCiw1aAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABFJREFUCJlj+M/AgBVhF/0PAH6/D/HkDxOGAAAAAElFTkSuQmCC)}.cropper-hide{display:block;height:0;position:absolute;width:0}.cropper-hidden{display:none!important}.cropper-move{cursor:move}.cropper-crop{cursor:crosshair}.cropper-disabled .cropper-drag-box,.cropper-disabled .cropper-face,.cropper-disabled .cropper-line,.cropper-disabled .cropper-point{cursor:not-allowed}"],
                        encapsulation: i0.ViewEncapsulation.None
                    },] },
        ];
        /** @nocollapse */
        CropperComponent.ctorParameters = function () { return []; };
        CropperComponent.propDecorators = {
            image: [{ type: i0.ViewChild, args: ['image',] }],
            imageUrl: [{ type: i0.Input }],
            settings: [{ type: i0.Input }],
            cropbox: [{ type: i0.Input }],
            loadImageErrorText: [{ type: i0.Input }],
            cropperOptions: [{ type: i0.Input }],
            export: [{ type: i0.Output }],
            ready: [{ type: i0.Output }]
        };
        return CropperComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var AngularCropperjsModule = (function () {
        function AngularCropperjsModule() {
        }
        AngularCropperjsModule.decorators = [
            { type: i0.NgModule, args: [{
                        imports: [
                            common.CommonModule
                        ],
                        declarations: [CropperComponent],
                        exports: [CropperComponent]
                    },] },
        ];
        return AngularCropperjsModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.AngularCropperjsService = AngularCropperjsService;
    exports.CropperComponent = CropperComponent;
    exports.AngularCropperjsModule = AngularCropperjsModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5ndWxhci1jcm9wcGVyanMudW1kLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9hbmd1bGFyLWNyb3BwZXJqcy9saWIvYW5ndWxhci1jcm9wcGVyanMuc2VydmljZS50cyIsIm5nOi8vYW5ndWxhci1jcm9wcGVyanMvbGliL2Nyb3BwZXIvY3JvcHBlci5jb21wb25lbnQudHMiLCJuZzovL2FuZ3VsYXItY3JvcHBlcmpzL2xpYi9hbmd1bGFyLWNyb3BwZXJqcy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBBbmd1bGFyQ3JvcHBlcmpzU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgRWxlbWVudFJlZiwgVmlld0NoaWxkLCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCBDcm9wcGVyIGZyb20gJ2Nyb3BwZXJqcyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSW1hZ2VDcm9wcGVyU2V0dGluZyB7XG4gICAgd2lkdGg6IG51bWJlcjtcbiAgICBoZWlnaHQ6IG51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJbWFnZUNyb3BwZXJSZXN1bHQge1xuICAgIGltYWdlRGF0YTogQ3JvcHBlci5JbWFnZURhdGE7XG4gICAgY3JvcERhdGE6IENyb3BwZXIuQ3JvcEJveERhdGE7XG4gICAgYmxvYj86IEJsb2I7XG4gICAgZGF0YVVybD86IHN0cmluZztcbn1cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhbmd1bGFyLWNyb3BwZXInLFxuICAgIHRlbXBsYXRlOiBgPCEtLSBDUk9QUEVSIFdSQVBQRVIgLS0+XG48ZGl2IGNsYXNzPVwiY3JvcHBlci13cmFwcGVyXCI+XG5cbiAgICA8IS0tIExPQURJTkcgLS0+XG4gICAgPGRpdiBjbGFzcz1cImxvYWRpbmctYmxvY2tcIiAqbmdJZj1cImlzTG9hZGluZ1wiPlxuICAgICAgICA8ZGl2IGNsYXNzPVwic3Bpbm5lclwiPjwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPCEtLSBMT0FEIEVSUk9SIC0tPlxuICAgIDxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC13YXJuaW5nXCIgKm5nSWY9XCJsb2FkRXJyb3JcIj57eyBsb2FkSW1hZ2VFcnJvclRleHQgfX08L2Rpdj5cblxuICAgIDwhLS0gQ1JPUFBFUiAtLT5cbiAgICA8ZGl2IGNsYXNzPVwiY3JvcHBlclwiPlxuICAgICAgICA8aW1nICNpbWFnZSBhbHQ9XCJpbWFnZVwiIFtzcmNdPVwiaW1hZ2VVcmxcIiAobG9hZCk9XCJpbWFnZUxvYWRlZCgkZXZlbnQpXCIgKGVycm9yKT1cImltYWdlTG9hZEVycm9yKCRldmVudClcIiAvPlxuICAgIDwvZGl2PlxuPC9kaXY+XG5gLFxuICAgIHN0eWxlczogW2A6aG9zdHtkaXNwbGF5OmJsb2NrfS5jcm9wcGVyIGltZ3ttYXgtd2lkdGg6MTAwJTttYXgtaGVpZ2h0OjEwMCU7aGVpZ2h0OmF1dG99LmNyb3BwZXItd3JhcHBlcntwb3NpdGlvbjpyZWxhdGl2ZTttaW4taGVpZ2h0OjgwcHh9LmNyb3BwZXItd3JhcHBlciAubG9hZGluZy1ibG9ja3twb3NpdGlvbjphYnNvbHV0ZTt0b3A6MDtsZWZ0OjA7d2lkdGg6MTAwJTtoZWlnaHQ6MTAwJX0uY3JvcHBlci13cmFwcGVyIC5sb2FkaW5nLWJsb2NrIC5zcGlubmVye3dpZHRoOjMxcHg7aGVpZ2h0OjMxcHg7bWFyZ2luOjAgYXV0bztib3JkZXI6MnB4IHNvbGlkIHJnYmEoOTcsMTAwLDE5MywuOTgpO2JvcmRlci1yYWRpdXM6NTAlO2JvcmRlci1sZWZ0LWNvbG9yOnRyYW5zcGFyZW50O2JvcmRlci1yaWdodC1jb2xvcjp0cmFuc3BhcmVudDstd2Via2l0LWFuaW1hdGlvbjo0MjVtcyBsaW5lYXIgaW5maW5pdGUgY3NzbG9hZC1zcGluO3Bvc2l0aW9uOmFic29sdXRlO3RvcDpjYWxjKDUwJSAtIDE1cHgpO2xlZnQ6Y2FsYyg1MCUgLSAxNXB4KTthbmltYXRpb246NDI1bXMgbGluZWFyIGluZmluaXRlIGNzc2xvYWQtc3Bpbn1ALXdlYmtpdC1rZXlmcmFtZXMgY3NzbG9hZC1zcGlue3Rvey13ZWJraXQtdHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpO3RyYW5zZm9ybTpyb3RhdGUoMzYwZGVnKX19QGtleWZyYW1lcyBjc3Nsb2FkLXNwaW57dG97LXdlYmtpdC10cmFuc2Zvcm06cm90YXRlKDM2MGRlZyk7dHJhbnNmb3JtOnJvdGF0ZSgzNjBkZWcpfX0vKiFcbiAqIENyb3BwZXIuanMgdjEuNC4xXG4gKiBodHRwczovL2Zlbmd5dWFuY2hlbi5naXRodWIuaW8vY3JvcHBlcmpzXG4gKlxuICogQ29weXJpZ2h0IDIwMTUtcHJlc2VudCBDaGVuIEZlbmd5dWFuXG4gKiBSZWxlYXNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2VcbiAqXG4gKiBEYXRlOiAyMDE4LTA3LTE1VDA5OjU0OjQzLjE2N1pcbiAqLy5jcm9wcGVyLWNvbnRhaW5lcntkaXJlY3Rpb246bHRyO2ZvbnQtc2l6ZTowO2xpbmUtaGVpZ2h0OjA7cG9zaXRpb246cmVsYXRpdmU7dG91Y2gtYWN0aW9uOm5vbmU7LXdlYmtpdC11c2VyLXNlbGVjdDpub25lOy1tb3otdXNlci1zZWxlY3Q6bm9uZTstbXMtdXNlci1zZWxlY3Q6bm9uZTt1c2VyLXNlbGVjdDpub25lfS5jcm9wcGVyLWNvbnRhaW5lciBpbWd7ZGlzcGxheTpibG9jaztoZWlnaHQ6MTAwJTtpbWFnZS1vcmllbnRhdGlvbjowZGVnO21heC1oZWlnaHQ6bm9uZSFpbXBvcnRhbnQ7bWF4LXdpZHRoOm5vbmUhaW1wb3J0YW50O21pbi1oZWlnaHQ6MCFpbXBvcnRhbnQ7bWluLXdpZHRoOjAhaW1wb3J0YW50O3dpZHRoOjEwMCV9LmNyb3BwZXItY2FudmFzLC5jcm9wcGVyLWNyb3AtYm94LC5jcm9wcGVyLWRyYWctYm94LC5jcm9wcGVyLW1vZGFsLC5jcm9wcGVyLXdyYXAtYm94e2JvdHRvbTowO2xlZnQ6MDtwb3NpdGlvbjphYnNvbHV0ZTtyaWdodDowO3RvcDowfS5jcm9wcGVyLWNhbnZhcywuY3JvcHBlci13cmFwLWJveHtvdmVyZmxvdzpoaWRkZW59LmNyb3BwZXItZHJhZy1ib3h7YmFja2dyb3VuZC1jb2xvcjojZmZmO29wYWNpdHk6MH0uY3JvcHBlci1tb2RhbHtiYWNrZ3JvdW5kLWNvbG9yOiMwMDA7b3BhY2l0eTouNX0uY3JvcHBlci12aWV3LWJveHtkaXNwbGF5OmJsb2NrO2hlaWdodDoxMDAlO291dGxpbmU6IzM5ZiBzb2xpZCAxcHg7b3ZlcmZsb3c6aGlkZGVuO3dpZHRoOjEwMCV9LmNyb3BwZXItZGFzaGVke2JvcmRlcjowIGRhc2hlZCAjZWVlO2Rpc3BsYXk6YmxvY2s7b3BhY2l0eTouNTtwb3NpdGlvbjphYnNvbHV0ZX0uY3JvcHBlci1kYXNoZWQuZGFzaGVkLWh7Ym9yZGVyLWJvdHRvbS13aWR0aDoxcHg7Ym9yZGVyLXRvcC13aWR0aDoxcHg7aGVpZ2h0OmNhbGMoMTAwJSAvIDMpO2xlZnQ6MDt0b3A6Y2FsYygxMDAlIC8gMyk7d2lkdGg6MTAwJX0uY3JvcHBlci1kYXNoZWQuZGFzaGVkLXZ7Ym9yZGVyLWxlZnQtd2lkdGg6MXB4O2JvcmRlci1yaWdodC13aWR0aDoxcHg7aGVpZ2h0OjEwMCU7bGVmdDpjYWxjKDEwMCUgLyAzKTt0b3A6MDt3aWR0aDpjYWxjKDEwMCUgLyAzKX0uY3JvcHBlci1jZW50ZXJ7ZGlzcGxheTpibG9jaztoZWlnaHQ6MDtsZWZ0OjUwJTtvcGFjaXR5Oi43NTtwb3NpdGlvbjphYnNvbHV0ZTt0b3A6NTAlO3dpZHRoOjB9LmNyb3BwZXItY2VudGVyOmFmdGVyLC5jcm9wcGVyLWNlbnRlcjpiZWZvcmV7YmFja2dyb3VuZC1jb2xvcjojZWVlO2NvbnRlbnQ6JyAnO2Rpc3BsYXk6YmxvY2s7cG9zaXRpb246YWJzb2x1dGV9LmNyb3BwZXItY2VudGVyOmJlZm9yZXtoZWlnaHQ6MXB4O2xlZnQ6LTNweDt0b3A6MDt3aWR0aDo3cHh9LmNyb3BwZXItY2VudGVyOmFmdGVye2hlaWdodDo3cHg7bGVmdDowO3RvcDotM3B4O3dpZHRoOjFweH0uY3JvcHBlci1mYWNlLC5jcm9wcGVyLWxpbmUsLmNyb3BwZXItcG9pbnR7ZGlzcGxheTpibG9jaztoZWlnaHQ6MTAwJTtvcGFjaXR5Oi4xO3Bvc2l0aW9uOmFic29sdXRlO3dpZHRoOjEwMCV9LmNyb3BwZXItZmFjZXtiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7bGVmdDowO3RvcDowfS5jcm9wcGVyLWxpbmV7YmFja2dyb3VuZC1jb2xvcjojMzlmfS5jcm9wcGVyLWxpbmUubGluZS1le2N1cnNvcjpldy1yZXNpemU7cmlnaHQ6LTNweDt0b3A6MDt3aWR0aDo1cHh9LmNyb3BwZXItbGluZS5saW5lLW57Y3Vyc29yOm5zLXJlc2l6ZTtoZWlnaHQ6NXB4O2xlZnQ6MDt0b3A6LTNweH0uY3JvcHBlci1saW5lLmxpbmUtd3tjdXJzb3I6ZXctcmVzaXplO2xlZnQ6LTNweDt0b3A6MDt3aWR0aDo1cHh9LmNyb3BwZXItbGluZS5saW5lLXN7Ym90dG9tOi0zcHg7Y3Vyc29yOm5zLXJlc2l6ZTtoZWlnaHQ6NXB4O2xlZnQ6MH0uY3JvcHBlci1wb2ludHtiYWNrZ3JvdW5kLWNvbG9yOiMzOWY7aGVpZ2h0OjVweDtvcGFjaXR5Oi43NTt3aWR0aDo1cHh9LmNyb3BwZXItcG9pbnQucG9pbnQtZXtjdXJzb3I6ZXctcmVzaXplO21hcmdpbi10b3A6LTNweDtyaWdodDotM3B4O3RvcDo1MCV9LmNyb3BwZXItcG9pbnQucG9pbnQtbntjdXJzb3I6bnMtcmVzaXplO2xlZnQ6NTAlO21hcmdpbi1sZWZ0Oi0zcHg7dG9wOi0zcHh9LmNyb3BwZXItcG9pbnQucG9pbnQtd3tjdXJzb3I6ZXctcmVzaXplO2xlZnQ6LTNweDttYXJnaW4tdG9wOi0zcHg7dG9wOjUwJX0uY3JvcHBlci1wb2ludC5wb2ludC1ze2JvdHRvbTotM3B4O2N1cnNvcjpzLXJlc2l6ZTtsZWZ0OjUwJTttYXJnaW4tbGVmdDotM3B4fS5jcm9wcGVyLXBvaW50LnBvaW50LW5le2N1cnNvcjpuZXN3LXJlc2l6ZTtyaWdodDotM3B4O3RvcDotM3B4fS5jcm9wcGVyLXBvaW50LnBvaW50LW53e2N1cnNvcjpud3NlLXJlc2l6ZTtsZWZ0Oi0zcHg7dG9wOi0zcHh9LmNyb3BwZXItcG9pbnQucG9pbnQtc3d7Ym90dG9tOi0zcHg7Y3Vyc29yOm5lc3ctcmVzaXplO2xlZnQ6LTNweH0uY3JvcHBlci1wb2ludC5wb2ludC1zZXtib3R0b206LTNweDtjdXJzb3I6bndzZS1yZXNpemU7aGVpZ2h0OjIwcHg7b3BhY2l0eToxO3JpZ2h0Oi0zcHg7d2lkdGg6MjBweH1AbWVkaWEgKG1pbi13aWR0aDo3NjhweCl7LmNyb3BwZXItcG9pbnQucG9pbnQtc2V7aGVpZ2h0OjE1cHg7d2lkdGg6MTVweH19QG1lZGlhIChtaW4td2lkdGg6OTkycHgpey5jcm9wcGVyLXBvaW50LnBvaW50LXNle2hlaWdodDoxMHB4O3dpZHRoOjEwcHh9fUBtZWRpYSAobWluLXdpZHRoOjEyMDBweCl7LmNyb3BwZXItcG9pbnQucG9pbnQtc2V7aGVpZ2h0OjVweDtvcGFjaXR5Oi43NTt3aWR0aDo1cHh9fS5jcm9wcGVyLXBvaW50LnBvaW50LXNlOmJlZm9yZXtiYWNrZ3JvdW5kLWNvbG9yOiMzOWY7Ym90dG9tOi01MCU7Y29udGVudDonICc7ZGlzcGxheTpibG9jaztoZWlnaHQ6MjAwJTtvcGFjaXR5OjA7cG9zaXRpb246YWJzb2x1dGU7cmlnaHQ6LTUwJTt3aWR0aDoyMDAlfS5jcm9wcGVyLWludmlzaWJsZXtvcGFjaXR5OjB9LmNyb3BwZXItYmd7YmFja2dyb3VuZC1pbWFnZTp1cmwoZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFCQUFBQUFRQVFNQUFBQWxQVzBpQUFBQUEzTkNTVlFJQ0FqYjRVL2dBQUFBQmxCTVZFWE16TXovLy8vVGpSVjJBQUFBQ1hCSVdYTUFBQXJyQUFBSzZ3R0NpdzFhQUFBQUhIUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkdhWEpsZDI5eWEzTWdRMU0yNkx5eWpBQUFBQkZKUkVGVUNKbGorTS9BZ0JWaEYvMFBBSDYvRC9Ia0R4T0dBQUFBQUVsRlRrU3VRbUNDKX0uY3JvcHBlci1oaWRle2Rpc3BsYXk6YmxvY2s7aGVpZ2h0OjA7cG9zaXRpb246YWJzb2x1dGU7d2lkdGg6MH0uY3JvcHBlci1oaWRkZW57ZGlzcGxheTpub25lIWltcG9ydGFudH0uY3JvcHBlci1tb3Zle2N1cnNvcjptb3ZlfS5jcm9wcGVyLWNyb3B7Y3Vyc29yOmNyb3NzaGFpcn0uY3JvcHBlci1kaXNhYmxlZCAuY3JvcHBlci1kcmFnLWJveCwuY3JvcHBlci1kaXNhYmxlZCAuY3JvcHBlci1mYWNlLC5jcm9wcGVyLWRpc2FibGVkIC5jcm9wcGVyLWxpbmUsLmNyb3BwZXItZGlzYWJsZWQgLmNyb3BwZXItcG9pbnR7Y3Vyc29yOm5vdC1hbGxvd2VkfWBdLFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgQ3JvcHBlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBAVmlld0NoaWxkKCdpbWFnZScpIGltYWdlOiBFbGVtZW50UmVmO1xuXG4gICAgQElucHV0KCkgaW1hZ2VVcmw6IGFueTtcbiAgICBASW5wdXQoKSBzZXR0aW5nczogSW1hZ2VDcm9wcGVyU2V0dGluZztcbiAgICBASW5wdXQoKSBjcm9wYm94OiBDcm9wcGVyLkNyb3BCb3hEYXRhO1xuICAgIEBJbnB1dCgpIGxvYWRJbWFnZUVycm9yVGV4dDogc3RyaW5nO1xuICAgIEBJbnB1dCgpIGNyb3BwZXJPcHRpb25zOiBhbnkgPSB7fTtcblxuICAgIEBPdXRwdXQoKSBleHBvcnQgPSBuZXcgRXZlbnRFbWl0dGVyPEltYWdlQ3JvcHBlclJlc3VsdD4oKTtcbiAgICBAT3V0cHV0KCkgcmVhZHkgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICBwdWJsaWMgaXNMb2FkaW5nOiBib29sZWFuID0gdHJ1ZTtcbiAgICBwdWJsaWMgY3JvcHBlcjogQ3JvcHBlcjtcbiAgICBwdWJsaWMgaW1hZ2VFbGVtZW50OiBIVE1MSW1hZ2VFbGVtZW50O1xuICAgIHB1YmxpYyBsb2FkRXJyb3I6IGFueTtcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJbWFnZSBsb2FkZWRcbiAgICAgKiBAcGFyYW0gZXZcbiAgICAgKi9cbiAgICBpbWFnZUxvYWRlZChldjogRXZlbnQpIHtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBVbnNldCBsb2FkIGVycm9yIHN0YXRlXG4gICAgICAgIHRoaXMubG9hZEVycm9yID0gZmFsc2U7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gU2V0dXAgaW1hZ2UgZWxlbWVudFxuICAgICAgICBjb25zdCBpbWFnZSA9IGV2LnRhcmdldCBhcyBIVE1MSW1hZ2VFbGVtZW50O1xuICAgICAgICB0aGlzLmltYWdlRWxlbWVudCA9IGltYWdlO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEFkZCBjcm9zc09yaWdpbj9cbiAgICAgICAgY29uc29sZS5sb2coJ3RoaXMuY3JvcHBlck9wdGlvbnMnLCB0aGlzLmNyb3BwZXJPcHRpb25zKTtcbiAgICAgICAgaWYgKHRoaXMuY3JvcHBlck9wdGlvbnMuY2hlY2tDcm9zc09yaWdpbikgaW1hZ2UuY3Jvc3NPcmlnaW4gPSAnYW5vbnltb3VzJztcblxuICAgICAgICAvL1xuICAgICAgICAvLyBJbWFnZSBvbiByZWFkeSBldmVudFxuICAgICAgICBpbWFnZS5hZGRFdmVudExpc3RlbmVyKCdyZWFkeScsICgpID0+IHtcbiAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAvLyBFbWl0IHJlYWR5XG4gICAgICAgICAgICB0aGlzLnJlYWR5LmVtaXQodHJ1ZSk7XG5cbiAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAvLyBVbnNldCBsb2FkaW5nIHN0YXRlXG4gICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuXG4gICAgICAgICAgICAvL1xuICAgICAgICAgICAgLy8gVmFsaWRhdGUgY3JvcGJveCBleGlzdGFuY2VcbiAgICAgICAgICAgIGlmICh0aGlzLmNyb3Bib3gpIHtcblxuICAgICAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAgICAgLy8gU2V0IGNyb3Bib3ggZGF0YVxuICAgICAgICAgICAgICAgIHRoaXMuY3JvcHBlci5zZXRDcm9wQm94RGF0YSh0aGlzLmNyb3Bib3gpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBTZXR1cCBhc3BlY3QgcmF0aW8gYWNjb3JkaW5nIHRvIHNldHRpbmdzXG4gICAgICAgIGxldCBhc3BlY3RSYXRpbyA9IE5hTjtcbiAgICAgICAgaWYgKHRoaXMuc2V0dGluZ3MpIHtcbiAgICAgICAgICAgIGNvbnN0IHsgd2lkdGgsIGhlaWdodCB9ID0gdGhpcy5zZXR0aW5ncztcbiAgICAgICAgICAgIGFzcGVjdFJhdGlvID0gd2lkdGggLyBoZWlnaHQ7XG4gICAgICAgIH1cblxuICAgICAgICAvL1xuICAgICAgICAvLyBTZXQgY3JvcCBvcHRpb25zXG4gICAgICAgIC8vIGV4dGVuZCBkZWZhdWx0IHdpdGggY3VzdG9tIGNvbmZpZ1xuICAgICAgICB0aGlzLmNyb3BwZXJPcHRpb25zID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICAgICAgICBhc3BlY3RSYXRpbyxcbiAgICAgICAgICAgIG1vdmFibGU6IGZhbHNlLFxuICAgICAgICAgICAgc2NhbGFibGU6IGZhbHNlLFxuICAgICAgICAgICAgem9vbWFibGU6IGZhbHNlLFxuICAgICAgICAgICAgdmlld01vZGU6IDEsXG4gICAgICAgICAgICBjaGVja0Nyb3NzT3JpZ2luOiB0cnVlXG4gICAgICAgIH0sIHRoaXMuY3JvcHBlck9wdGlvbnMpO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFNldCBjcm9wcGVyanNcbiAgICAgICAgdGhpcy5jcm9wcGVyID0gbmV3IENyb3BwZXIoaW1hZ2UsIHRoaXMuY3JvcHBlck9wdGlvbnMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEltYWdlIGxvYWQgZXJyb3JcbiAgICAgKiBAcGFyYW0gZXZlbnRcbiAgICAgKi9cbiAgICBpbWFnZUxvYWRFcnJvcihldmVudDogYW55KSB7XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gU2V0IGxvYWQgZXJyb3Igc3RhdGVcbiAgICAgICAgdGhpcy5sb2FkRXJyb3IgPSB0cnVlO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIFVuc2V0IGxvYWRpbmcgc3RhdGVcbiAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBFeHBvcnQgY2FudmFzXG4gICAgICogQHBhcmFtIGJhc2U2NFxuICAgICAqL1xuICAgIGV4cG9ydENhbnZhcyhiYXNlNjQ/OiBhbnkpIHtcblxuICAgICAgICAvL1xuICAgICAgICAvLyBHZXQgYW5kIHNldCBpbWFnZSwgY3JvcCBhbmQgY2FudmFzIGRhdGFcbiAgICAgICAgY29uc3QgaW1hZ2VEYXRhID0gdGhpcy5jcm9wcGVyLmdldEltYWdlRGF0YSgpO1xuICAgICAgICBjb25zdCBjcm9wRGF0YSA9IHRoaXMuY3JvcHBlci5nZXRDcm9wQm94RGF0YSgpO1xuICAgICAgICBjb25zdCBjYW52YXMgPSB0aGlzLmNyb3BwZXIuZ2V0Q3JvcHBlZENhbnZhcygpO1xuICAgICAgICBjb25zdCBkYXRhID0geyBpbWFnZURhdGEsIGNyb3BEYXRhIH07XG5cbiAgICAgICAgLy9cbiAgICAgICAgLy8gQ3JlYXRlIHByb21pc2UgdG8gcmVzb2x2ZSBjYW52YXMgZGF0YVxuICAgICAgICBjb25zdCBwcm9taXNlID0gbmV3IFByb21pc2UocmVzb2x2ZSA9PiB7XG5cbiAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAvLyBWYWxpZGF0ZSBiYXNlNjRcbiAgICAgICAgICAgIGlmIChiYXNlNjQpIHtcblxuICAgICAgICAgICAgICAgIC8vXG4gICAgICAgICAgICAgICAgLy8gUmVzb2x2ZSBwcm9taXNlIHdpdGggZGF0YVVybFxuICAgICAgICAgICAgICAgIHJldHVybiByZXNvbHZlKHtcbiAgICAgICAgICAgICAgICAgICAgZGF0YVVybDogY2FudmFzLnRvRGF0YVVSTCgnaW1hZ2UvcG5nJylcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhbnZhcy50b0Jsb2IoYmxvYiA9PiByZXNvbHZlKHsgYmxvYiB9KSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEVtaXQgZXhwb3J0IGRhdGEgd2hlbiBwcm9taXNlIGlzIHJlYWR5XG4gICAgICAgIHByb21pc2UudGhlbihyZXMgPT4ge1xuICAgICAgICAgICAgdGhpcy5leHBvcnQuZW1pdChPYmplY3QuYXNzaWduKGRhdGEsIHJlcykpO1xuICAgICAgICB9KTtcbiAgICB9XG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ3JvcHBlckNvbXBvbmVudCB9IGZyb20gJy4vY3JvcHBlci9jcm9wcGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgQ29tbW9uTW9kdWxlXG4gICAgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtDcm9wcGVyQ29tcG9uZW50XSxcbiAgICBleHBvcnRzOiBbQ3JvcHBlckNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgQW5ndWxhckNyb3BwZXJqc01vZHVsZSB7IH1cbiJdLCJuYW1lcyI6WyJJbmplY3RhYmxlIiwiRXZlbnRFbWl0dGVyIiwiQ29tcG9uZW50IiwiVmlld0VuY2Fwc3VsYXRpb24iLCJWaWV3Q2hpbGQiLCJJbnB1dCIsIk91dHB1dCIsIk5nTW9kdWxlIiwiQ29tbW9uTW9kdWxlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7QUFBQTtRQU9FO1NBQWlCOztvQkFMbEJBLGFBQVUsU0FBQzt3QkFDVixVQUFVLEVBQUUsTUFBTTtxQkFDbkI7Ozs7O3NDQUpEOzs7Ozs7O0FDQUE7UUErREk7a0NBVitCLEVBQUU7MEJBRWQsSUFBSUMsZUFBWSxFQUFzQjt5QkFDdkMsSUFBSUEsZUFBWSxFQUFFOzZCQUVSLElBQUk7U0FLZjs7OztRQUVqQixtQ0FBUTs7O1lBQVI7YUFDQzs7Ozs7Ozs7OztRQU1ELHNDQUFXOzs7OztZQUFYLFVBQVksRUFBUztnQkFBckIsaUJBNERDOzs7Z0JBeERHLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDOztnQkFJdkIsSUFBTSxLQUFLLElBQUcsRUFBRSxDQUFDLE1BQTBCLEVBQUM7Z0JBQzVDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDOzs7Z0JBSTFCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCO29CQUFFLEtBQUssQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDOzs7Z0JBSTFFLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUU7Ozs7O29CQUc1QixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzs7Ozs7b0JBSXRCLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDOzs7b0JBSXZCLElBQUksS0FBSSxDQUFDLE9BQU8sRUFBRTs7Ozs7d0JBSWQsS0FBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUM3QztpQkFDSixDQUFDLENBQUM7O2dCQUlILElBQUksV0FBVyxHQUFHLEdBQUcsQ0FBQztnQkFDdEIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNmLHdCQUFRLGdCQUFLLEVBQUUsa0JBQU0sQ0FBbUI7b0JBQ3hDLFdBQVcsR0FBRyxLQUFLLEdBQUcsTUFBTSxDQUFDO2lCQUNoQzs7OztnQkFLRCxJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7b0JBQ2hDLFdBQVcsYUFBQTtvQkFDWCxPQUFPLEVBQUUsS0FBSztvQkFDZCxRQUFRLEVBQUUsS0FBSztvQkFDZixRQUFRLEVBQUUsS0FBSztvQkFDZixRQUFRLEVBQUUsQ0FBQztvQkFDWCxnQkFBZ0IsRUFBRSxJQUFJO2lCQUN6QixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzs7O2dCQUl4QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDMUQ7Ozs7Ozs7Ozs7UUFNRCx5Q0FBYzs7Ozs7WUFBZCxVQUFlLEtBQVU7OztnQkFJckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7OztnQkFJdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDMUI7Ozs7Ozs7Ozs7UUFNRCx1Q0FBWTs7Ozs7WUFBWixVQUFhLE1BQVk7Z0JBQXpCLGlCQStCQzs7Z0JBM0JHLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUM7O2dCQUM5QyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxDQUFDOztnQkFDL0MsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDOztnQkFDL0MsSUFBTSxJQUFJLEdBQUcsRUFBRSxTQUFTLFdBQUEsRUFBRSxRQUFRLFVBQUEsRUFBRSxDQUFDOztnQkFJckMsSUFBTSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsVUFBQSxPQUFPOzs7b0JBSS9CLElBQUksTUFBTSxFQUFFOzs7d0JBSVIsT0FBTyxPQUFPLENBQUM7NEJBQ1gsT0FBTyxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDO3lCQUN6QyxDQUFDLENBQUM7cUJBQ047b0JBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLE9BQU8sQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLENBQUMsR0FBQSxDQUFDLENBQUM7aUJBQzVDLENBQUMsQ0FBQzs7O2dCQUlILE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO29CQUNaLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzlDLENBQUMsQ0FBQzthQUNOOztvQkF6S0pDLFlBQVMsU0FBQzt3QkFDUCxRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixRQUFRLEVBQUUseWZBZ0JiO3dCQUNHLE1BQU0sRUFBRSxDQUFDLDIySUFRbTdHLENBQUM7d0JBQzc3RyxhQUFhLEVBQUVDLG9CQUFpQixDQUFDLElBQUk7cUJBQ3hDOzs7Ozs0QkFHSUMsWUFBUyxTQUFDLE9BQU87K0JBRWpCQyxRQUFLOytCQUNMQSxRQUFLOzhCQUNMQSxRQUFLO3lDQUNMQSxRQUFLO3FDQUNMQSxRQUFLOzZCQUVMQyxTQUFNOzRCQUNOQSxTQUFNOzsrQkF4RFg7Ozs7Ozs7QUNBQTs7OztvQkFJQ0MsV0FBUSxTQUFDO3dCQUNOLE9BQU8sRUFBRTs0QkFDTEMsbUJBQVk7eUJBQ2Y7d0JBQ0QsWUFBWSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7d0JBQ2hDLE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO3FCQUM5Qjs7cUNBVkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=