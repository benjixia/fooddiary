import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'main', loadChildren: './modules/tabs/tabs.module#TabsModule' },
  { path: 'login', loadChildren: './modules/login/login.module#LoginModule' },
  { path: 'my-account', loadChildren: './modules/my-account/my-account.module#MyAccountModule' },
  { path: 'near-by', loadChildren: './modules/near-by/near-by.module#NearByModule' },
  { path: 'crop-image', loadChildren: './modules/crop-image/crop-image.module#CropImageModule' },
  { path: 'picture-menu', loadChildren: './modules/picture-menu/picture-menu.module#PictureMenuModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
