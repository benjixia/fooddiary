import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  user: User;

  constructor() { }

  ngOnInit() {
    this.initSampleUser();
  }

  initSampleUser() {
    this.user = {} as User;
    this.user.Username = "d3ad_bunny";
    this.user.Email = "db@example.com";
    this.user.FirstName = "Yui";
    this.user.LastName = "Ishikawa";
    this.user.FullName = "Yui Ishikawa";
    this.user.SmallPhotoUrl = 'https://i.imgur.com/oW1dGDI.jpg';
    this.user.SmallPhoto = null;
    this.user.FullPhotoUrl = null;
    this.user.FullPhoto = null;
  }

}
