import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
 
import { InlineRatingComponent } from './inline-rating.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  declarations: [InlineRatingComponent],
  exports: [InlineRatingComponent],
})
export class InlineRatingModule { }
