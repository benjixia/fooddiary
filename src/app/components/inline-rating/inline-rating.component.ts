import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-inline-rating',
  templateUrl: './inline-rating.component.html',
  styleUrls: ['./inline-rating.component.scss']
})
export class InlineRatingComponent implements OnInit {
  @Input() array: Array<number>;

  constructor() { }

  ngOnInit() {
  }

}
