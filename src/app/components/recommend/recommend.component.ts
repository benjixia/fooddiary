import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SlideOptions } from 'src/app/models/slide-options.model';

@Component({
  selector: 'app-recommend',
  templateUrl: './recommend.component.html',
  styleUrls: ['./recommend.component.scss']
})
export class RecommendComponent implements OnInit {
  @Input() slideOpts: SlideOptions;
  @Input() items: Array<{imgUrl: string, dishName?: string}>;
  @Input() showTitle: boolean;
  @Input() showOverlay: boolean;
  @Input() customClass: string;
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.showTitle === null || typeof this.showTitle == 'undefined') {
      this.showTitle = true;
    }
    if (this.showOverlay === null || typeof this.showOverlay == 'undefined') {
      this.showOverlay = true;
    }
  }

  applyClass(item: string){
    if (item) {
      let classArray = item.split(" ");
      if (classArray.length > 0) {
        let cssOverride = {};
        for (var i = 0; i < classArray.length; i++) {
          cssOverride[classArray[i]] = true;
        }
        return cssOverride;
      } else {
        return null;
      }
    }
  }

  clickEvent(item) {
    this.onClick.emit(item);
  }

}
