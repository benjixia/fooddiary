import { Component, OnInit, Input } from '@angular/core';
import { SnapShotItem } from 'src/app/models/snap.shot.item.model';
import { Constants } from 'src/app/models/constants.model';

@Component({
  selector: 'app-snap-shots',
  templateUrl: './snap-shots.component.html',
  styleUrls: ['./snap-shots.component.scss']
})
export class SnapShotsComponent implements OnInit {
  @Input() items: Array<SnapShotItem>;
  defaultImageUrl: string = Constants.default404ImageUrl;
  constructor() { }

  ngOnInit() {
    this.items.forEach(item => {
      if (!item.user.SmallPhotoUrl) {
        item.user.SmallPhotoUrl = '../../../assets/images/default-profile.png';
      }
      item.ratingArray = new Array<number>();
      for (var i=0; i<Math.round(item.rating); i++) {
        item.ratingArray.push(1);
      }
    });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

}
