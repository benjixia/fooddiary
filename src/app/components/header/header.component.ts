import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RootScope } from 'src/app/models/root.scope.model';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Router } from '@angular/router';
import { Constants } from 'src/app/models/constants.model';
import { CropImageService } from 'src/app/services/crop-image-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() user: User;
  @Output() imageCapture: EventEmitter<any> = new EventEmitter();

  showSearch: boolean = false;

  constructor(private camera: Camera, 
              private rootScope: RootScope,
              private router: Router,
              private cropImageService: CropImageService) { 

  }

  refreshSearch(searchValue: boolean) {
    this.showSearch = !searchValue;
  }

  captureCamera() {
    if (this.rootScope.useNativeHttp) {

      /** correct orientation for now, there's an issue on certain android devices  where image orientation are not consistent **/
      const options: CameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit: false,
        correctOrientation: true,
        saveToPhotoAlbum: true,
        sourceType: 1
      }
      
      this.camera.getPicture(options).then((imageData) => {
        this.router.navigate(['crop-image', {fileUrl: imageData, fileType: Constants.fileTypeFileUrl}]);
      }, (err) => {
        console.log(err);
        alert('Something wrong, please try again.');
      });
    } else {
      alert('camera not available on pc, using mock image instead');
      var placeholderImage = Constants.placeHolderBase64Image;
      this.cropImageService.saveImage(placeholderImage);
      this.router.navigate(['crop-image', {fileUrl: null, fileType: Constants.fileTypeBase64}]);
    }
  }
}
