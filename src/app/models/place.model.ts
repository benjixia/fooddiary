export class Place {
    Id: string;
    Name: string;
    Description?: string;
    ImageUrl?: string;
    Address?: string;
    Rating?: number;
    Type?: string;
    Array?: Array<number>;

    constructor(private placeData?: any) {
        this.Id = placeData.Id;
        this.Name = placeData.Name;
        this.Description = placeData.Description;
        this.ImageUrl = placeData.ImageUrl;
        this.Address = placeData.Address;
        this.Rating = placeData.Rating;
        this.Type = placeData.Type;
        this.Array = this.createRatingArray(this.Rating);
    }

    // helper function to create array based on rating for the inline rating component
    createRatingArray(rating: number) {
        var outputArray: Array<number> = new Array<number>();
        var wholeStar: number = Math.floor(rating);
        var halfStar: number = rating % 1;
        outputArray = new Array(wholeStar).fill(1);
        if (halfStar <= 0.29) {
            outputArray.push(0);
        } else if (halfStar > 0.29 && halfStar < 0.79) {
            outputArray.push(2);
        } else {
            outputArray.push(1);
        }
        if ((5 - wholeStar - 1) > 0) {
            outputArray = outputArray.concat(new Array<number>((5 - wholeStar - 1)).fill(0));
        }
        return outputArray;
    }
}