import { Platform } from "@ionic/angular";
import { Storage } from '@ionic/storage';
import { Constants } from 'src/app/models/constants.model';
import { Injectable } from '@angular/core';
import { User } from "./user.model";

@Injectable({
  providedIn: 'root',
})
export class RootScope {
    userAuthenticated : boolean = false;
    useNativeHttp : boolean = false;
    user : User;
    isLoading : boolean = false;
    isAsyncProcessing: boolean = false;
    isIos: boolean = false;
    isAndroid: boolean = false;
    isMobile: boolean = false;

    constructor(private platform: Platform, private storage: Storage) {
        
        // check platform
        if (platform.is('ios')) {
            this.isIos = true;
            this.isMobile = true;
        } else if (platform.is('android')) {
            this.isAndroid = true;
            this.isMobile = true;
        }

        // set if we use native http (only works on phones)
        if (this.isMobile) {
            this.useNativeHttp = true;
        }

        // check if user logged in
        this.storage.get(Constants.accessTokenKey).then((val) => {
            if (val) {
                this.userAuthenticated = true;
            }
        });
    }
}