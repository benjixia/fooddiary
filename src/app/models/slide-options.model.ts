export class SlideOptions {
    loop?: boolean;
    spaceBetween?: number; 
    mode?: string; 
    width?: number; height?: number;
    autoHeight?: boolean; 
    effect?: string; 
    slidesPerView?: number; 
    shortSwipes?: boolean; 
    longSwipes?: boolean;
    zoom?: boolean;

    constructor() {

    }
}