import { Constants } from "./constants.model";

export class User {
    Username: string;
    Password?: string;
    Email: string;
    FirstName: string;
    LastName: string;
    FullName: string;
    SmallPhotoUrl: string;
    SmallPhoto: string;
    FullPhotoUrl: string;
    FullPhoto: string;

    constructor(private userData?: any) {
        this.Username = userData.Username;
        this.Email = userData.Email;
        this.FirstName = userData.FirstName;
        this.LastName = userData.LastName;
        this.FullName = userData.Name;
        this.SmallPhotoUrl = userData.SmallPhotoUrl;
        this.SmallPhoto = Constants.base64Prefix + userData.SmallPhoto;
        this.FullPhotoUrl = userData.FullPhotoUrl;
        this.FullPhoto = Constants.base64Prefix + userData.FullPhoto;
    }
}
/** we are not storing password here, just a placeholder **/