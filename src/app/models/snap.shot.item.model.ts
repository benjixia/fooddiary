import { User } from './user.model';

export class SnapShotItem {
    imgUrl?: string;
    title?: string;
    subTitle?: string;
    rating?: number;
    user?: User;
    ratingArray?: Array<number>;
}