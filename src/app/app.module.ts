import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { RootScope } from 'src/app/models/root.scope.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { AccountService } from 'src/app/services/account-service.service';
import { LoginService } from 'src/app/services/login-service.service';
import { LoadingService } from 'src/app/services/loading-service.service';
import 'web-photo-filter';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FilterPipe } from './pipes/filter.pipe';
import { AuthInterceptor } from './Interceptor/auth.interceptor';
import { CropImageService } from './services/crop-image-service.service';
import { MenuService } from './services/menu-service.service';
import 'hammerjs';
import { ImageService } from './services/image-service.service';
import { ToastService } from './services/toast-service.service';

@NgModule({
  declarations: [AppComponent, FilterPipe],
  entryComponents: [],
  imports: [BrowserModule, 
            IonicModule.forRoot(), 
            AppRoutingModule, 
            HttpClientModule,
            IonicStorageModule.forRoot(),
            BrowserAnimationsModule
          ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    HTTP,
    AccountService,
    LoginService,
    LoadingService,
    CropImageService,
    ImageService,
    MenuService,
    ToastService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    FilterPipe,
    Camera,
    WebView,
    Base64
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
