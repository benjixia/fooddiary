import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { SnapShotsComponent } from '../components/snap-shots/snap-shots.component';
import { HeaderModule } from 'src/app/components/header/header.module';
import { RecommendModule } from '../components/recommend/recommend.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    RecommendModule,
    RouterModule.forChild([
      { path: '', component: HomePage }
    ])
  ],
  declarations: [HomePage, SnapShotsComponent]
})
export class HomePageModule{
}
