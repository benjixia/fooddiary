import { Component, ViewChild, OnInit } from '@angular/core';
import { HeaderComponent } from '../components/header/header.component';
import { RouterOutlet, Router, ActivatedRoute, ActivationEnd} from '@angular/router';
import { filter } from 'rxjs/operators';

import { SnapShotItem } from 'src/app/models/snap.shot.item.model';
import { User } from '../models/user.model';
import { AccountService } from '../services/account-service.service';
import { RootScope } from '../models/root.scope.model';
import { LoadingService } from '../services/loading-service.service';
import { CropImageService } from '../services/crop-image-service.service';
import { Constants } from '../models/constants.model';
import { SlideOptions } from '../models/slide-options.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  slideOpts: SlideOptions = {loop: false, effect: "fade", spaceBetween: 8, slidesPerView: 4, zoom: false};
  items: Array<{imgUrl: string, dishName?: string}>;
  snapShotItem: SnapShotItem;
  snapShotItems: Array<SnapShotItem>;
  user: User;
  sampleDishNames: Array<string>;

  cameraImage = null;

  constructor(public accountService: AccountService,
              public loadingService: LoadingService,
              private rootScope: RootScope,
              private router: Router,
              private cropImageService: CropImageService,
              private route: ActivatedRoute) {
    
  }

  ngOnInit() {

    // starting loading
    this.loadingService.presentLoading();
    
    // get current user
    this.getCurrentUser();

    // make up some sample data
    this.initRecommendItems();
    this.initSampleUser();
    this.initSnapShot();

    this.router.events.pipe(
      filter(event => event instanceof ActivationEnd)
      ).subscribe((event: ActivationEnd) => {
        if (event.snapshot.url.length > 0) {
          if (event.snapshot.url[0].path == Constants.homePath) {
            this.createNewPosts();
          }
        }
      });

    // end loading
    this.loadingService.dismissLoading();
  }

  cropImage(imgUrl) {
    this.cameraImage = imgUrl;
  }

  createNewPosts() {

    // add any image captured from camera
    var image = this.cropImageService.retrieveImage();
    if (image) {
      this.createNewPost(image);

      // clear the image cache since we don't need it anymore
      this.cropImageService.resetImageCache();
    }
  }

  createNewPost(imgUrl) {

    // set cameraImage to null this method of showing the main page needs to be changed later
    this.cameraImage = null;

    // need to upload image to server first, then we can use the url for post, but we're going to skip that right now
    var rating = 4.3;
    var postText = 'Just some sample text to text out the new post.'
    this.createNewSnapShot(imgUrl, rating, postText);
  }

  createNewSnapShot(imgUrl, rating, postText) {
    this.snapShotItem = {imgUrl: null, title: null, subTitle: null, rating: null, user: this.rootScope.user};
    this.snapShotItem.rating = rating;
    this.snapShotItem.imgUrl = imgUrl;
    this.snapShotItem.subTitle = postText;
    this.snapShotItem.subTitle = this.snapShotItem.subTitle.replace(/\n/g, '<br/>');
    this.snapShotItem.ratingArray = new Array<number>();
    for (var i=0; i<Math.round(this.snapShotItem.rating); i++) {
      this.snapShotItem.ratingArray.push(1);
    }
    let copy = Object.assign({}, this.snapShotItem);
    this.snapShotItems.unshift(copy);
  }

  initRecommendItems() {
    this.items = [];
    this.sampleDishNames = ["Grilled Chicken Sandwhich", "Parmigiana Beef", "Margherita Pizza", "Spicy Burrito", "House Pancake"];
    for (var i=1; i<6; i++) {
      this.items.push({imgUrl: "../../assets/images/place-holder/dish" + i + ".jpeg", dishName: this.sampleDishNames[i-1]});
    }
  }

  initSampleUser() {
    this.user = {} as User;
    this.user.Username = "d3ad_bunny";
    this.user.Email = "db@example.com";
    this.user.FirstName = "Yui";
    this.user.LastName = "Ishikawa";
    this.user.FullName = "Yui Ishikawa";
    this.user.SmallPhotoUrl = 'https://i.imgur.com/oW1dGDI.jpg';
    this.user.SmallPhoto = null;
    this.user.FullPhotoUrl = null;
    this.user.FullPhoto = null;
  }

  initSnapShot() {
    this.snapShotItem = {imgUrl: null, title: null, subTitle: null, rating: null, user: this.user};
    this.snapShotItem.subTitle = "Hot damn this place was good! \nI am going to have to be honest and say that this place is probably one of the best places I have ever eaten, HANDS DOWN! \nThe first picture is a short rib hash with a poached egg and the second is a rosemary focaccia bread served with what probably is the most yummy butter, olive oil and tapenade! Please try this place you most surely won't be disappointed.... \nFive out of five stars no question. I would give a sixth for great service and for the owner coming to talk to us. I didn\'t get to try any of their yummy cocktails (not drinking right now) but the ladies that were with me did and they raced about them. I will be back next week, count on it";
    this.snapShotItem.subTitle = this.snapShotItem.subTitle.replace(/\n/g, '<br/>');
    this.snapShotItems = new Array<SnapShotItem>();
    for (var i=0; i<5; i++) {
      this.snapShotItem.rating = 2 + Math.random()*3;
      this.snapShotItem.imgUrl = '../../assets/images/place-holder/snap-shot-placeholder' + (i+1) + '.jpeg';
      let copy = Object.assign({}, this.snapShotItem);
      this.snapShotItems.push(copy);
    }

    // add one more to test default placeholder image
    this.snapShotItem.rating = 2 + Math.random()*3;
    this.snapShotItem.imgUrl = null;
    let copy = Object.assign({}, this.snapShotItem);
    this.snapShotItems.push(copy);
  }

  getCurrentUser() {
    this.accountService.getLoggedInUser().subscribe(res => {
      
      this.user = this.rootScope.user;

    }, err => {
      
      console.log(err);
    });
  }

}
