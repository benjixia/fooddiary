import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Storage } from "@ionic/storage";
import { Constants } from '../models/constants.model';
import { from } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(public storage: Storage) {
    }

    /** add authorization header on all calls **/
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let promise = this.storage.get(Constants.accessTokenKey);
        return from(promise).pipe(mergeMap(token => {
            let clonedReq = this.getToken(request, token);
            return next.handle(clonedReq);
        }));
    }

    private getToken(request: HttpRequest<any>, token: any) {
        if (token) {
            let req: HttpRequest<any>;
            req = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            })
            return req;
        } else {
            return request;
        }
    }
}