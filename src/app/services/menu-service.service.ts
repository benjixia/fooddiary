import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootScope } from '../models/root.scope.model';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Constants } from '../models/constants.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  endpoint : string;
  httpOptions : any;
  currentMenu;

  constructor(private http: HttpClient, 
              private nativeHttp: HTTP,
              private rootScope: RootScope) {

  }

  getMenu(locationId: string) {
    var promise = new Observable((observer) => {
        if (this.rootScope.useNativeHttp) {
          this.nativeHttp.get(Constants.nativeHttpDomainUrl + Constants.menuUrl + locationId, {}, {})
          .then(data => {

              // pass the data along
              var menu = JSON.parse(data.data);
              observer.next(menu);
          })
          .catch(error => {
              console.log(error.error);
              observer.error(error);
          });
        } else {
          this.endpoint = Constants.menuUrl + locationId;
          this.http.get<any>(this.endpoint, this.httpOptions).subscribe(res=>{

              // pass the data along
              observer.next(res);
          }, err => {
              console.log(err);
              observer.error(err);
          });
        }
      });
    return promise;
  }

  getCurrentMenu() {
    return this.currentMenu;
  }

  saveCurrentMenu(menu) {
    this.currentMenu = menu;
  }
}
