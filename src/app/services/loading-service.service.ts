import { Injectable } from '@angular/core';
import { RootScope } from 'src/app/models/root.scope.model';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private rootScope: RootScope) {
  }

  presentLoading() {
    this.rootScope.isLoading = true;
  }

  dismissLoading() {
    this.rootScope.isLoading = false;
  }

  presentAsyncLoading() {
    this.rootScope.isAsyncProcessing = true;
  }

  dismissAsyncLoading() {
    this.rootScope.isAsyncProcessing = false;
  }

}
