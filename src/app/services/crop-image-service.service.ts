import { Injectable } from '@angular/core';
import { strict } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class CropImageService {
  base64Image: string;

  constructor() { }

  saveImage(imageData) {
    this.base64Image = imageData;
  }

  retrieveImage() {
    return this.base64Image;
  }

  resetImageCache() {
    this.base64Image = null;
  }
}
