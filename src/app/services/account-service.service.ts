import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { Constants } from 'src/app/models/constants.model';
import { Storage } from '@ionic/storage';
import { RootScope } from '../models/root.scope.model';
import { HTTP } from '@ionic-native/http/ngx';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  endpoint : string;
  httpOptions : any;

  constructor(private http: HttpClient, 
              private nativeHttp: HTTP, 
              private storage: Storage, 
              private rootScope: RootScope,
              private router: Router,
              private webView: WebView) {
    
  }

  getLoggedInUser(): Observable<any> {
      var promise = new Observable((observer) => {
        this.storage.get(Constants.accessTokenKey).then((val) => {
            this.storage.get(Constants.userNameKey).then((userNameVal) => {
                if (this.rootScope.useNativeHttp) {

                    // set the auth header on app refresh
                    this.nativeHttp.setHeader("*", "Authorization", "Bearer " + val);
                    this.nativeHttp.setHeader("*", "Content-Type", 'application/json');
                    this.nativeHttp.setDataSerializer('json');
                    this.nativeHttp.get(Constants.nativeHttpDomainUrl + Constants.userUrl + userNameVal, {}, {})
                    .then(data => {

                        // set the user with user data
                        this.rootScope.user = new User(JSON.parse(data.data));
                        observer.next(data);
                    })
                    .catch(error => {
                        console.log(error.error);
                        observer.error(error);
                        alert('User session expired or user logged out, please log in again');
                        this.router.navigate(['/login']);
                    });
                } else {
                    this.endpoint = Constants.userUrl + userNameVal;
                    this.http.get<User>(this.endpoint, this.httpOptions).subscribe(res=>{

                        // set the user with user data
                        this.rootScope.user = new User(res);
                        observer.next(res);
                    }, err => {
                        console.log(err);
                        observer.error(err);
                        alert('User session expired or user logged out, please log in again');
                        this.router.navigate(['/login']);
                    });
                }
            });
        });
      });
      return promise;
  }
}
