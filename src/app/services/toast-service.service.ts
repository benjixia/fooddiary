import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) {
  }

  async presentToast(message?: string) {
    var toastMessage = '';
    if (message) {
        toastMessage = message;
    }
    const toast = await this.toastController.create({
      message: toastMessage,
      duration: 1000,
      position: 'top'
    });
    toast.present();
  }

  async presentToastWithCloseButton(message?: string, closeButtonText?: string) {
    var toastMessage = '';
    var toastCloseButtonText = 'Done';
    if (message) {
        toastMessage = message;
    }
    if (closeButtonText) {
        toastCloseButtonText = closeButtonText;
    }
    const toast = await this.toastController.create({
      message: toastMessage,
      showCloseButton: true,
      position: 'top',
      closeButtonText: toastCloseButtonText
    });
    toast.present();
  }

}
