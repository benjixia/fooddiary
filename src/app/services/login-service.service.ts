import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { Constants } from 'src/app/models/constants.model';
import { RootScope } from 'src/app/models/root.scope.model';
import { Storage } from '@ionic/storage';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  endpoint : string;
  httpOptions : any;

  constructor(private http: HttpClient, 
              private nativeHttp: HTTP, 
              private platform: Platform, 
              private rootScope: RootScope,
              private storage: Storage) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })
    };
  }

  isUserLoggedIn(): Observable<any> {
    var promise = new Observable((observer) => {
      this.storage.get(Constants.accessTokenKey).then((val) => {
        if (val) {
          this.rootScope.userAuthenticated = true;
        }
        observer.next(this.rootScope.userAuthenticated);
      });
    });
    return promise;
  }

  authenticate(userName: string, password: string): Observable<any> {
    
    // initiate request body for authentication
    var body = {grant_type: Constants.oauthGrantType,
                client_id: Constants.clientId,
                client_secret: Constants.clientSecret,
                username: userName,
                password: password};

    // init user in root scope
    this.setUser(userName);

    // proceed with login API
    if (this.rootScope.useNativeHttp) {
      var promise = new Observable((observer) => {
        this.nativeHttp.setDataSerializer("utf8");
        this.nativeHttp.post(Constants.nativeHttpLoginUrl + Constants.loginUrl, this.convertToFromUrlEncode(body), {'Content-Type': Constants.contentTypeFormUrlencoded})
        .then(data => {
          var responseObj = JSON.parse(data.data);
          this.nativeHttp.setHeader(Constants.nativeHttpDomainUrl, "Authorization", "Bearer " + responseObj.access_token);
          this.nativeHttp.setHeader("*", "Authorization", "Bearer " + responseObj.access_token);
          observer.next(data);
        })
        .catch(error => {
          console.log(error.data);
          observer.error(error);
        });
      });
      return promise;
    } else {
      var promise = new Observable((observer) => {
        this.endpoint = Constants.loginUrl;
        this.http.post<any>(this.endpoint, this.convertToFromUrlEncode(body), this.httpOptions).subscribe(res => {
          observer.next(res);
        }, err => {
          observer.error(err);
          console.log('err:', err);
        });
      });
      return promise;
    }
  }

  logOut() {

    // clear storage of access token and username
    this.storage.remove(Constants.userNameKey);
    this.storage.remove(Constants.accessTokenKey);

    // clear rootscope
    this.rootScope.userAuthenticated = false;
    this.rootScope.user = {} as User;
    this.rootScope.user.Username = null;
    this.rootScope.user.Password = null;
  }

  // helper functions
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private convertToFromUrlEncode(obj: any) {
    var str = [];
    for (var key in obj) {
         if (obj.hasOwnProperty(key)) {
               str.push(encodeURIComponent(key) + "=" + encodeURIComponent(obj[key]))
         }
    }
    return str.join("&");
  }

  private setUser(userName: string) {
    this.rootScope.user = {} as User;
    this.rootScope.user.Username = userName;
  }
}
