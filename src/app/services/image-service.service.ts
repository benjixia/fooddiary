import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RootScope } from '../models/root.scope.model';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Constants } from '../models/constants.model';
import { Router } from '@angular/router';
import { CropImageService } from './crop-image-service.service';
import { LoadingService } from './loading-service.service';
import { ToastService } from './toast-service.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  endpoint : string;
  httpOptions : any;

  constructor(private rootScope: RootScope,
              private http: HttpClient, 
              private nativeHttp: HTTP,
              private router: Router,
              private cropImageServce: CropImageService,
              private loadingService: LoadingService,
              private toastService: ToastService) { }
  
  uploadImage(fileUrl: string, type: string, purpose: string, recordId: string, crop?: boolean, retUrl?: string, message?: string) {
    
    if (crop === null || typeof crop == 'undefined') {
      crop = false;
    }

    if (crop) {
      this.cropImage(fileUrl, type, purpose, recordId, retUrl, message);
    } else {

      // start async process
      this.loadingService.presentAsyncLoading();
      this.uploadCroppedImage(fileUrl, type, purpose, recordId).subscribe(res => {
        if (message) {

          // dismiss async loading and present the toast if there's messages
          this.loadingService.dismissAsyncLoading();
          this.toastService.presentToast(message);
        }
      }, err => {

      });
    }
  }

  cropImage(fileUrl: string, type: string, purpose: string, recordId: string, retUrl: string, message?: string) {
    if (type == Constants.fileTypeBase64) {

      // send to crop page via crop-image service
      this.cropImageServce.saveImage(fileUrl);
      this.router.navigate(['crop-image', {fileUrl: null, fileType: Constants.fileTypeBase64, recordId: recordId, purpose: purpose, retUrl: retUrl, message: message}]).then(res => {
      
      });
    } else if (type == Constants.fileTypeFileUrl) {

      // send to crop page, will not work with extrernal urls
      this.router.navigate(['crop-image', {fileUrl: fileUrl, fileType: Constants.fileTypeFileUrl, recordId: recordId, purpose: purpose, retUrl: retUrl, message: message}]).then(res => {

      });
    } else {
      alert('cropping of external url is not supported');
    }
  }

  uploadCroppedImage(fileUrl: string, type: string, purpose: string, recordId: string) {
    var promise = new Observable((observer) => {
      let imageFileUrl = fileUrl;
      if (type == Constants.fileTypeBase64) {
        if (fileUrl.includes(Constants.base64SearchPrefix)) {
          imageFileUrl = fileUrl;
        } else {
          imageFileUrl = Constants.base64Prefix + fileUrl;
        }
      }
      var payload = {file: imageFileUrl, purpose: purpose, recordId: recordId};
      this.uploadAPI(payload).subscribe(res => {
        observer.next(res);
      }, err => {
        observer.error(err);
      });
    });
    return promise;
  }

  uploadAPI(payload) {
    console.log('check payload: ', payload);
    var promise = new Observable((observer) => {
        if (this.rootScope.useNativeHttp) {
          this.nativeHttp.post(Constants.nativeHttpDomainUrl + Constants.uploadImageUrl,payload, {})
          .then(data => {

              // pass the data along
              var menu = JSON.parse(data.data);
              console.log(menu);
              observer.next(menu);
          })
          .catch(error => {
              console.log(error);
              observer.error(error);
          });
        } else {
          this.endpoint = Constants.uploadImageUrl;
          this.http.post<any>(this.endpoint, payload, this.httpOptions).subscribe(res=>{

              // pass the data along
              console.log(res);
              observer.next(res);
          }, err => {
              console.log(err);
              observer.error(err);
          });
        }
      });
    return promise;
  }
}
