import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs';
import { RootScope } from './models/root.scope.model';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash: boolean = true;
  loadingMessages: Array<string> = new Array<string>();
  loadingMessage: string;
  isLoading: boolean = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private rootScope: RootScope
  ) {
    this.initializeLoadingMessages();
    this.initializeApp();
  }

  initializeLoadingMessages() {
    this.loadingMessages.push('Food is warming up...');
    this.loadingMessages.push('Cooking the patties...');
    this.loadingMessages.push('Rice is steaming...');
    this.loadingMessages.push('Let it simmer a bit longer...');
    this.loadingMessages.push('Something smells good...');
    this.loadingMessage = this.loadingMessages[Math.floor(Math.random() * this.loadingMessages.length)];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      timer(3000).subscribe(() => this.showSplash = false);
    });
  }
}
