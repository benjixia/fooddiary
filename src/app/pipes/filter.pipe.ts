import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], filter: Object, strict: boolean): any {
    if (!items || !filter) {
      return items;
    }
    if (typeof strict == 'undefined' || strict == null) {
      strict = false;
    }
    return items.filter(item => this.addToView(item, filter, strict));
  }

  addToView(item, filter, strict) {
    var keys = Object.keys(filter);
    var output = true;
    for (var i=0; i<keys.length; i++) {

      // separate based on type
      if (typeof filter[keys[i]] == "number") {

        // return items above or equal numeric value
        output = output && (item[keys[i]] >= filter[keys[i]]);
      } else {

        // return items matching (or partially matching) string value
        if (strict) {
          output = output && ((item[keys[i]] == filter[keys[i]]) || filter[keys[i]] === "" || filter[keys[i]] === null);
        } else {
          output = output && ((item[keys[i]].indexOf(filter[keys[i]]) !== -1) || filter[keys[i]] === "" || filter[keys[i]] === null);
        }
      }
    }
    return output;
  }

}
