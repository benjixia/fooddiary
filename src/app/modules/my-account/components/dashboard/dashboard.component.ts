import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login-service.service';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading-service.service';
import { RootScope } from 'src/app/models/root.scope.model';
import { AccountService } from 'src/app/services/account-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user: User;

  constructor(public loadingService: LoadingService,
              private rootScope: RootScope,
              private router: Router, 
              private loginService: LoginService,
              private accountService: AccountService) { 

              }

  ngOnInit() {
    this.initUser();
  }

  initUser() {
    this.user = this.rootScope.user;
  }

  logOut() {
    var confirmation = confirm('You\'re about to log out of the app, are you sure?');
    if (confirmation) {
      this.loginService.logOut();
      this.router.navigate(['/login']);
    }
  }
}
