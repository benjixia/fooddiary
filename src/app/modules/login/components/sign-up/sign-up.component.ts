import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { User } from 'src/app/models/user.model';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-sign-up',
  templateUrl: 'sign-up.component.html',
  styleUrls: ['sign-up.component.scss']
})

export class SignUpComponent {
  user: User;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private alertController: AlertController 
  ) {
    this.initialize();
  }

  initialize() {

    // do nothing for now
  }

  async signUp() {
    this.emailVerify(['/login', 'login']);
  }

  async emailVerify(template) {
    let alert = await this.alertController.create({
      header: 'Sucess!',
      message: 'Please check your email for a message from us. Once your email address is verified, you can sign into our services.',
      buttons: [{
        text: 'Ok',
        handler: () => {
          if (template != null) {
            this.router.navigate(template);
          }
        }
      }]
    });
    await alert.present();
  }
}
