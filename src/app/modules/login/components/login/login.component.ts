import { Component } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';

import { User } from 'src/app/models/user.model';

import { Platform, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { LoginService } from 'src/app/services/login-service.service';
import { Constants } from 'src/app/models/constants.model';
import { Storage } from '@ionic/storage';
import { RootScope } from 'src/app/models/root.scope.model';
import { LoadingService } from 'src/app/services/loading-service.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})

export class LoginComponent {
  user: User;
  loginResponse: any;

  constructor(
    public loginService: LoginService,
    public loadingService: LoadingService,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private rootScope: RootScope
  ) {
    this.initialize();
    this.loadingService.dismissLoading();
  }

  initialize() {

    // do nothing for now
    this.initUser();
  }

  initUser() {

    // init new user
    this.user = {} as User;
    this.user.Username = null;
    this.user.Password = null;

    // check if user logged in already?
    this.loginService.isUserLoggedIn().subscribe(res => {
      if (res) {

        // user already logged in, then update user object and go to main page
        this.router.navigate(['/main']);
      }
    });
  }

  login() {

    // show loading 
    this.loadingService.presentLoading();

    // do login logic
    this.loginService.authenticate(this.user.Username, this.user.Password).subscribe(res => {
      var loginSuccess = false;
      if (res) {
        if (this.rootScope.useNativeHttp) {
          if (res.status == 200) {

            loginSuccess = true;
            var data = JSON.parse(res.data);

            // set access token to storage
            this.rootScope.userAuthenticated = true;
            this.storage.set(Constants.accessTokenKey, data.access_token);
            this.storage.set(Constants.userNameKey, this.user.Username);
          }
        } else if (res.access_token) {
          loginSuccess = true;

          // set access token to storage
          this.rootScope.userAuthenticated = true;
          this.storage.set(Constants.accessTokenKey, res.access_token);
          this.storage.set(Constants.userNameKey, this.user.Username);
        }
      } 
      
      if (loginSuccess) {
        
        // reset form data
        var resetForm = <HTMLFormElement>document.getElementById("loginForm");
        resetForm.reset();
        this.loadingService.dismissLoading();
        this.router.navigate(['/main']);
      } else {
        this.loadingService.dismissLoading();
        alert('User name and/or password incorrect. Please try again');
      }
    }, err => {
      this.loadingService.dismissLoading();
      alert('User name and/or password incorrect. Please try again');
    });
  }

  signUp() {
    this.router.navigate(['/login','signup']);
  }

  // work around for form submit not picking up key events
  keyDownFunction(event: any) {

    // detect enter
    if(event.keyCode == 13) {
      this.login();
    }
  }
}
