import { Component, OnInit, ViewChild, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CropperComponent  } from 'angular-cropperjs-local';
import { Router, ActivatedRoute } from '@angular/router';
import { Constants } from 'src/app/models/constants.model';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { CropImageService } from 'src/app/services/crop-image-service.service';
import { SlideOptions } from 'src/app/models/slide-options.model';
import { LoadingService } from 'src/app/services/loading-service.service';
import { Location } from '@angular/common';
import { ImageService } from 'src/app/services/image-service.service';

@Component({
  selector: 'app-crop-image',
  templateUrl: './crop-image.component.html',
  styleUrls: ['./crop-image.component.scss'],
  animations: [
    trigger('visibilityChanged', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0, display: 'none' })),
      transition('* => *', animate('500ms'))
    ])
  ]
})
export class CropImageComponent implements OnInit {
  @Input() myImage;
  @Output() cropComplete: EventEmitter<any> = new EventEmitter();

  // filter slides option
  slideOpts: SlideOptions = {loop: false, effect: "fade", spaceBetween: 8, slidesPerView: 4, zoom: false};
  items: Array<{imgUrl: string, dishName?: string}> = new Array<{imgUrl: string, dishName?: string}>();
  showFilterTitle: boolean = false;
  showOverlay: boolean = false;

  // filter options
  selectedFilter = null;
  image = '';
  level = 1;
  result: HTMLElement;
  myImageCopy;
  myFilterImage;
  filterImageMap: Map<string, string> = new Map<string, string>();
  previewInProgress: boolean = false;
  filterPreviewImage;
  completedPreivewFilters: Array<string> = new Array<string>();
  @ViewChild('filterContent') filterContent: ElementRef;

  // random UI things
  firstTimeContentChecked: boolean = true;
  showOptions: boolean = false;
  isCropping: boolean = false;
  filterNames: Array<string> = ["original", "sepia", "polaroid", "cookie", "vintage", "koda", "bgr", "technicolor", "greyscale"];

  /** cropping stuff */
  @ViewChild('angularCropper') public angularCropper: CropperComponent;
  
  cropperOptions: any;
  croppedImage = null;
  scaleValX = 1;
  scaleValY = 1;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private webView: WebView,
              private cropImageService: CropImageService,
              private loadingService: LoadingService,
              private location: Location,
              private imageService: ImageService) {
    this.cropperOptions = {
      viewMode: 3,
      dragMode: 'none',
      aspectRatio: 4/3,
      autoCrop: true,
      movable: false,
      zoomable: false,
      scalable: false,
      rotatable: true,
      minCropBoxWidth: 250,
      minCropBoxHeight: 187,
      toggleDragModeOnDblclick: false,
      autoCropArea: 0.8,
      center: false
    };
  }

  ngOnInit() {

    // put spinner up on init
    this.loadingService.presentLoading();

    var base64Image;
    var fileType = this.route.snapshot.paramMap.get(Constants.fileTypeKey);

    // get the file type first, then grab url if needed
    if (fileType == Constants.fileTypeBase64) {
      
      // already base 64, grab the data from crop image serve
      base64Image = this.cropImageService.retrieveImage();
      this.cropImageService.resetImageCache();
    } else {
      
      // get the image from url if possible
      if (this.route.snapshot.paramMap.get(Constants.fileUrlKey)) {
        if (fileType == Constants.fileTypeFileUrl) {
        
          // convert the file uri to base64
          base64Image = this.webView.convertFileSrc(this.route.snapshot.paramMap.get(Constants.fileUrlKey));
        } else if (fileType == Constants.fileTypeExternalUrl) {
  
          // external url not suppported
          alert('external url not supported right now.');
        }
      } else {
        alert('file type not specified');
      }
    }

    this.myImage = base64Image;
    this.myImageCopy = this.myImage;
    this.myFilterImage = this.myImage;
    
    this.initFilterItems();
  }

  initFilterItems() {
    this.previewInProgress = true;
  }

  addToItems(item) {
    this.items.push(item);
  }

  cancelCrop() {

    // cancel the action
    // go back to previous state if specified, otherwise go back to main
    if (this.route.snapshot.paramMap.get(Constants.retUrlKey)) {
      this.router.navigate([this.route.snapshot.paramMap.get(Constants.retUrlKey)]);
    } else {
      this.router.navigate(['/main']);
    }
  }

  doneCrop() {

    // use crop image service to save the image
    var base64Image = this.angularCropper.cropper.getCroppedCanvas().toDataURL();

    // upload cropped image
    if (this.route.snapshot.paramMap.get(Constants.recordIdKey) && this.route.snapshot.paramMap.get(Constants.purposeKey)) {
      this.imageService.uploadImage(base64Image, Constants.fileTypeBase64, this.route.snapshot.paramMap.get(Constants.purposeKey), this.route.snapshot.paramMap.get(Constants.recordIdKey), null, null, this.route.snapshot.paramMap.get(Constants.messageKey));
    } else {

      // save the image for the home page
      this.cropImageService.saveImage(base64Image);
    }

    // destory cropper before moving on
    this.angularCropper.cropper.destroy();

    // go back to previous state if specified, otherwise go back to main
    if (this.route.snapshot.paramMap.get(Constants.retUrlKey)) {
      this.router.navigate([this.route.snapshot.paramMap.get(Constants.retUrlKey)]);
    } else {
      this.router.navigate(['/main']);
    }
  }

  /** cropping stuff */
  reset() {
    this.angularCropper.cropper.reset();
  }
 
  clear() {
    this.angularCropper.cropper.clear();
  }
 
  rotate(degrees) {
    this.angularCropper.cropper.rotate(degrees);
  }
 
  zoom(zoomIn: boolean) {
    let factor = zoomIn ? 0.1 : -0.1;
    this.angularCropper.cropper.zoom(factor);
  }
 
  scaleX() {
    this.scaleValX = this.scaleValX * -1;
    this.angularCropper.cropper.scaleX(this.scaleValX);
  }
 
  scaleY() {
    this.scaleValY = this.scaleValY * -1;
    this.angularCropper.cropper.scaleY(this.scaleValY);
  }
 
  move(x, y) {
    this.angularCropper.cropper.move(x, y);
  }
 
  save() {
    let croppedImgB64String: string = this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg', (100 / 100));
    this.croppedImage = croppedImgB64String;
  }

  processFilterPreview(filter) {
    // get image for filter
    this.filterPreviewImage = this.saveImage();

    // add to filter map
    this.addToFilterMap(filter, this.filterPreviewImage);
    
    // add item to list
    var item = {imgUrl: this.filterPreviewImage, dishName: filter};
    this.addToItems(item);
    
    this.completedPreivewFilters.push(filter);

    // keep processing filters until completed preview fitler array becomes the same length as the original filter name array
    if (this.completedPreivewFilters.length == (this.filterNames.length)) {
      this.previewInProgress = false;

      // reset the real image to null filter
      this.filter(null);

      // dismiss loading
      this.loadingService.dismissLoading();

      // show options
      this.showOptions = true;
    } else {
      this.filter(this.filterNames[this.completedPreivewFilters.length]);
    }
  }

  // filter functions
  imageLoaded(event, filter) {

    // get result after image loads;
    this.result = event.detail.result;

    // process image differently if preview vs actual image
    if (this.previewInProgress) {

      // call post processing if a filter is defined, otherwise add original image and kickoff the preview cycle
      if (filter) {
        this.processFilterPreview(filter);
      } else {

        // no filter, start filter
        this.completedPreivewFilters.push('original');

        // set image data to map
        this.addToFilterMap('original', this.myImage);

        // add item to list
        var item = {imgUrl: this.myImage, dishName: 'original'};
        this.addToItems(item);

        // start the next filter
        this.filter(this.filterNames[1]);
      }
    } else {

      // only do actual image processing if filter is not null
      if (filter) {
        this.myImage = this.saveImage();

        // need to update the angular cropperjs library to fix the replace issue
        this.angularCropper.cropper.replace(this.myImage, true);
      }

      // dismiss loading
      this.loadingService.dismissLoading();
    }
  }

  // add filtered image to map to help with performance
  addToFilterMap(filterName, fitlerImage) {
    this.filterImageMap.set(filterName, fitlerImage);
  }

  filter(selected, level?) {
    this.selectedFilter = selected;
    this.level = level ? level : 1;
  }
 
  saveImage() {
    var output;
      
    // do nothing if no filter 
    if (this.selectedFilter) {
      let canvas = this.result as HTMLCanvasElement;

      try {
        let base64Image = canvas.toDataURL('image/jpeg', 1.0);
        output = base64Image;
      }
      catch(e) {
        console.log(e);
      }
    }
    
    // return the base64image
    return output;
  }

  applyFilter(event) {

    // only continue if filter is not the same as now
    if (this.selectedFilter !== event.dishName) {

      // show loading 
      this.loadingService.presentLoading();

      // change image and reset the cropper
      if (this.filterImageMap.has(event.dishName)) {
        this.myImage = this.filterImageMap.get(event.dishName);
        this.angularCropper.cropper.replace(this.myImage, true);
      } else {
        alert('something wrong with filter...');
      }

      // dismiss loading
      this.loadingService.dismissLoading();
    }
  }

  keepAlive() {
    this.isCropping = true;
  }

  toggleOptions(options) {
    if (!this.isCropping) {
      
      this.showOptions = !options;
    }

    // reset the cropping indicator
    this.isCropping = false;
  }
}
