import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
 
import { CropImageComponent } from './components/crop-image/crop-image.component';
import { AngularCropperjsModule } from 'angular-cropperjs-local'
import { CropImageRoutingModule } from './crop-image-routing.module';
import { RecommendModule } from 'src/app/components/recommend/recommend.module';
import 'web-photo-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AngularCropperjsModule,
    CropImageRoutingModule,
    RecommendModule
  ],
  declarations: [CropImageComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [CropImageComponent],
})
export class CropImageModule { }
