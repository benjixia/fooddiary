import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from 'src/app/components/menu/menu.component';
 
const routes: Routes = [
  {
    path: 'tab',
    component: MenuComponent,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: 'src/app/home/home.module#HomePageModule'  
          }
        ]
      },
      {
        path: 'near-by',
        children: [
          {
            path: '',
            loadChildren: 'src/app/modules/near-by/near-by.module#NearByModule'
          }
        ]
      },
      {
        path: 'menu',
        children: [
          {
            path: '',
            loadChildren: 'src/app/modules/my-account/my-account.module#MyAccountModule'
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tab/home',
    pathMatch: 'full'
  }
];
 
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsRoutingModule { }