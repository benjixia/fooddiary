import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
 
import { TabsRoutingModule } from './tabs-routing.module';
import { MenuComponent } from 'src/app/components/menu/menu.component';
import { HomePageModule } from 'src/app/home/home.module';
import { MyAccountModule } from 'src/app/modules/my-account/my-account.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsRoutingModule,
    HomePageModule,
    MyAccountModule
  ],
  declarations: [MenuComponent]
})
export class TabsModule { }
