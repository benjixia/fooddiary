import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NearByListComponent } from './near-by-list.component';
import { HeaderModule } from 'src/app/components/header/header.module';
import { IonicModule } from '@ionic/angular';
import { InlineRatingModule } from 'src/app/components/inline-rating/inline-rating.module';
import { NearByListCardsComponent } from '../near-by-list-cards/near-by-list-cards.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NearByFilterComponent } from '../near-by-filter/near-by-filter.component';

@NgModule({
  declarations: [NearByListComponent, NearByListCardsComponent, NearByFilterComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    InlineRatingModule,
    RouterModule.forChild([
      {
        path: '',
        component: NearByListComponent
      }
    ])
  ],
  entryComponents: [
    NearByFilterComponent
  ]
})
export class NearByListModule { }
