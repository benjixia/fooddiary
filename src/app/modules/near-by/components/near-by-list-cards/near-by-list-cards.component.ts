import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Place } from 'src/app/models/place.model';
import { Constants } from 'src/app/models/constants.model';

@Component({
  selector: 'app-near-by-list-cards',
  templateUrl: './near-by-list-cards.component.html',
  styleUrls: ['./near-by-list-cards.component.scss']
})
export class NearByListCardsComponent implements OnInit {
  @Input() itemsView: Array<Place>;
  @Output() itemClicked: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  emitEvent(locationId) {
    this.itemClicked.emit(locationId);
  }
}
