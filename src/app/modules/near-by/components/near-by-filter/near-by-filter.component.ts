import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-near-by-filter',
  templateUrl: './near-by-filter.component.html',
  styleUrls: ['./near-by-filter.component.scss']
})
export class NearByFilterComponent implements OnInit {
  @Input() typeOptions: Array<string>;
  @Input() hourOptions: Array<Object>;
  @Input() ratingOptions: Array<Object>;
  @Input() filterObj;

  constructor(private modalController: ModalController) { }

  ngOnInit() {
  }

  applyFilter(apply) {
    if (apply) {
      this.modalController.dismiss(this.filterObj);
    } else {
      this.modalController.dismiss();
    }
  }

  resetFilter() {
    this.filterObj.Rating = null;
    this.filterObj.Type = null;
    this.filterObj.Hours = null;
  }

}
