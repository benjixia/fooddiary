import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NearByRoutingModule } from './near-by-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NearByRoutingModule
  ]
})
export class NearByModule { }
