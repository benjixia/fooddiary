import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NearByListComponent } from './components/near-by-list/near-by-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'near-by-list', pathMatch: 'full' },
  { path: 'near-by-list', loadChildren: 'src/app/modules/near-by/components/near-by-list/near-by-list.module#NearByListModule' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NearByRoutingModule { }
