import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PictureMenuComponent } from './components/picture-menu/picture-menu.component';
import { PictureMenuRoutingModule } from './picture-menu-routing.module';
import { NgxGalleryModule } from 'ngx-gallery-local';
import { MenuFeedbackFormComponent } from './components/menu-feedback-form/menu-feedback-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PictureMenuComponent, MenuFeedbackFormComponent],
  imports: [
    CommonModule,
    IonicModule,
    NgxGalleryModule,
    PictureMenuRoutingModule,
    FormsModule
  ],
  entryComponents: [
    MenuFeedbackFormComponent
  ]
})
export class PictureMenuModule { }
