import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PictureMenuComponent } from './components/picture-menu/picture-menu.component'

const routes: Routes = [
  { path: '', redirectTo: 'menu', pathMatch: 'full' },
  { path: 'menu', component: PictureMenuComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PictureMenuRoutingModule { }