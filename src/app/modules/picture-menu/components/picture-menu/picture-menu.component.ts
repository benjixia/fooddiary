import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/services/menu-service.service';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery-local';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RootScope } from 'src/app/models/root.scope.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImageService } from 'src/app/services/image-service.service';
import { Constants } from 'src/app/models/constants.model';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { MenuFeedbackFormComponent } from '../menu-feedback-form/menu-feedback-form.component';

@Component({
  selector: 'app-picture-menu',
  templateUrl: './picture-menu.component.html',
  styleUrls: ['./picture-menu.component.scss'],
  animations: [
    trigger('visibilityChanged', [
      state('shown', style({ opacity: 1 })),
      state('hidden', style({ opacity: 0, display: 'none' })),
      transition('* => *', animate('500ms'))
    ])
  ]
})
export class PictureMenuComponent implements OnInit {
  currentMenu;
  menuSections: Array<string> = ['Appetizers', 'Entree', 'Specialty', 'Dessert', 'Drinks'];
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  showMenu = {};
  menu404ImageUrl: string = Constants.menu404ImageUrl;

  constructor(public modalController: ModalController,
              private menuService: MenuService,
              private rootScope: RootScope,
              private camera: Camera,
              private imageService: ImageService,
              private router: Router) { }

  ngOnInit() {
    this.currentMenu = this.menuService.getCurrentMenu();

    this.galleryOptions = [
        {
            preview: true,
            image: false,
            previewFullscreen: false,
            imageArrows: this.enableArrows(), 
            imageSwipe: this.enableSwipe(), 
            thumbnailsArrows: this.enableArrows(), 
            thumbnailsSwipe: this.enableSwipe(), 
            previewArrows: this.enableArrows(),
            previewSwipe: this.enableSwipe(),
            thumbnailsPercent: 25, 
            thumbnailsRows: 1,
            thumbnailsColumns: 4, 
            thumbnailsMoveSize: 4,
            thumbnailsMargin: 0, 
            thumbnailMargin: 2,
            width: '100%',
            height: '100px',
            imageAnimation: NgxGalleryAnimation.Slide,
        }
    ];

    this.galleryImages = [];

    if (this.currentMenu) {
      this.initShowMenu();
    }
  }

  initShowMenu() {
    for (var i=0;i<this.currentMenu.length;i++) {
      this.showMenu[this.currentMenu[i].MenuType] = true;
    }
  }

  toggleMenu(menuType) {
    var isShow = this.showMenu[menuType];
    this.showMenu[menuType] = !isShow;
  }

  enableArrows() {
    if (this.rootScope.isMobile) {
      return false;
    } else {
      return true;
    }
  }

  enableSwipe() {
    if (this.rootScope.isMobile) {
      return true;
    } else {
      return false;
    }
  }

  meow(index, menuItemArray) {
    var menuItem = menuItemArray[index];
    var confirmation = confirm('Would you like to upload a picture for ' + menuItem.Name + ' ? \nThe resturant owner could pick your picture as the public image for this dish.');
    if (confirmation) {

      // go into picture picking mode
      if (this.rootScope.useNativeHttp) {

        /** correct orientation for now, there's an issue on certain android devices  where image orientation are not consistent **/
        const options: CameraOptions = {
          quality: 50,
          destinationType: this.camera.DestinationType.FILE_URI,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          allowEdit: false,
          correctOrientation: true,
          sourceType: 0
        }
        
        this.camera.getPicture(options).then((imageData) => {

          // upload data to server
          var retUrl = this.router.url;
          this.imageService.uploadImage(imageData, Constants.fileTypeFileUrl, Constants.purposeMenu, menuItem.Id, true, retUrl, Constants.menuPictureSubmissionSucessMessage);
        }, (err) => {
          console.log(err);
          alert('Something wrong, please try again.');
        });
      } else {
        alert('camera/ gallery not available on pc, using placeholder image instead');
        var placeholderImage = Constants.placeHolderBase64Image;
        var retUrl = this.router.url;
        this.imageService.uploadImage(placeholderImage, Constants.fileTypeBase64, Constants.purposeMenu, menuItem.Id, true, retUrl, Constants.menuPictureSubmissionSucessMessage);
      }
    }
  }

  initGallaryImages(menuItemArray) {
    var outputArray = [];
    if (menuItemArray) {
      for (var i=0; i<menuItemArray.length; i++) {
        var imageUrl = Constants.default404ImageUrl;
        if (menuItemArray[i].ImageUrl) {
          imageUrl = menuItemArray[i].ImageUrl;
        }
        var item = {
                    small: imageUrl,
                    medium: imageUrl,
                    big: imageUrl,
                    description: menuItemArray[i].Description
                  };
        outputArray.push(item);
      }
    }
    return outputArray;
  }

  initAddMenuItemForm(section: string) {

    // code to show modal for adding menu item
  }

  async showFeedbackForm() {

    const modal = await this.modalController.create({
      component: MenuFeedbackFormComponent,
      componentProps: { },
      cssClass: "half-screen-modal",
      showBackdrop: true,
      backdropDismiss: true,
      animated: true
    });

    modal.onDidDismiss()
      .then((data) => {
        if (data.data) {
          
          // send data out to server
        }
    });

    return await modal.present();
  }
}
