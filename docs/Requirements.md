# Food [Diary|Up|Journal|-ie?]
### Description
An app for food adventurers!

### Requirements
1. Member can take picture of food, upload them, and add tags to the picture.
2. Member can associate the picture of food with a menu item name -- this can be custom input or from official list provided by the restaurant.
3. Member and non-member can view full menu of restaurants with picture of menu item next to it.
4. Member and non-member can sort and search for a menu item from a restaurant or from nearby locations. The results will show up as relevant food item and their picture and restaurant name.
5. Member can review menu item and restaurant.
6. Member can create "foodie" group with other member(s) to share pictures and chat.
7. Member have a "I'm feeling lucky" option to get matched to a food item or restaurant based on their previous preferences.
8. Member can request food delivery service for a menu item using a third party food delivery service.
9. Member can request reservation for a restaurant through the app or through third party reservation service.

### edits by bxia
everything after and including requirement 6 should not be in the MVP, 1 - 5 defines a set of requirements to allow users to see food items in a menu everywhere they go and gauge how good it is, 6 - 9 applies to different usecase(s).
requirement 1 - 5 should be ordered as such so order of devlopement makes sense:

1. Member and non-member can view full menu of restaurants with picture of menu item next to it.
2. Member can take picture of food, upload them, and add tags to the picture.
3. Member can associate the picture of food with a menu item name -- this can be custom input or from official list provided by the restaurant.
4. Member and non-member can sort and search for a menu item from a restaurant or from nearby locations. The results will show up as relevant food item and their picture and restaurant name.
5. Member can review menu item and restaurant.